package com.tim7.sparkuy;

import androidx.test.runner.AndroidJUnit4;
import androidx.test.rule.ActivityTestRule;
import androidx.test.runner.AndroidJUnitRunner;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static androidx.test.espresso.action.ViewActions.typeText;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static androidx.test.espresso.action.ViewActions.closeSoftKeyboard;

import static androidx.test.espresso.Espresso.onView;
@RunWith(AndroidJUnit4.class)
public class RegisterActivityTest {
    @Rule
    public ActivityTestRule<Register> mActivity = new ActivityTestRule<>(Register.class);


    private String email = "email@user";
    private String nama = "user2";
    private String password = "123";
    private String noHp = "123";


    @Test
    public void changeTextEmail(){
        onView(withId(R.id.email_text)).perform(typeText(email), closeSoftKeyboard());
    }
    @Test
    public void changeTextName(){
        onView(withId(R.id.name_text)).perform(typeText(nama), closeSoftKeyboard());
    }


    @Test
    public void changeTextPassword(){
        onView(withId(R.id.password_text)).perform(typeText(password), closeSoftKeyboard());
    }

    @Test
    public void changeTextHandphone(){
        onView(withId(R.id.handphone_text)).perform(typeText(noHp), closeSoftKeyboard());
    }

}