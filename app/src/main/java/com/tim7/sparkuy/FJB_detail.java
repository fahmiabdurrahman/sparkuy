package com.tim7.sparkuy;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;

import com.tim7.sparkuy.FJB_List;
import com.tim7.sparkuy.R;

//import static com.example.iseng3.MainActivity.EXTRA_DESKRIPSI;
//import static com.example.iseng3.MainActivity.EXTRA_HARGA;
//import static com.example.iseng3.MainActivity.EXTRA_IMAGE;
//import static com.example.iseng3.MainActivity.EXTRA_NAMABARANG;


public class FJB_detail extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fjb_detail);

        Intent intent = getIntent();
        FJB_List fjb_list = intent.getParcelableExtra("Example Item");
        int imageRes = fjb_list.getmImageResource();
        String barang = fjb_list.getmNamaBarang();
        String deskripsi = fjb_list.getmDeskripsi();
        String harga = fjb_list.getmHarga();

        ImageView imageView = findViewById(R.id.imageDetail);
        imageView.setImageResource(imageRes);

        TextView textView1 = findViewById(R.id.namaBarangDetail);
        textView1.setText(barang);

        TextView textView2 = findViewById(R.id.deskripsiDetail);
        textView2.setText(deskripsi);

        TextView textView3 = findViewById(R.id.hargaDetail);
        textView3.setText(harga);
        //String imageBarang = intent.getStringExtra(EXTRA_IMAGE);
        // String namaBarang = intent.getStringExtra(EXTRA_NAMABARANG);
        // String deskripsiBarang = intent.getStringExtra(EXTRA_DESKRIPSI);
        // String hargaBarang = intent.getStringExtra(EXTRA_HARGA);

        //ImageView detailImage = findViewById(R.id.imageDetail);
        // TextView namaBarangDetail = findViewById(R.id.namaBarangDetail);
        // TextView deskripsiDetail = findViewById(R.id.deskripsiDetail);
        // TextView hargaDetail = findViewById(R.id.hargaDetail);

        // assert imageBarang != null;
        //detailImage.setImageResource(Integer.parseInt(imageBarang));
        // namaBarangDetail.setText(namaBarang);
        // deskripsiDetail.setText(deskripsiBarang);
        // hargaDetail.setText(hargaBarang);


    }
}
