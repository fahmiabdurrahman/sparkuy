package com.tim7.sparkuy;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.SearchView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;

public class FJB extends AppCompatActivity  {
    private FloatingActionButton buttonFJBku;

    private ArrayList<FJB_List> mFJBList;
    private RecyclerView mRecyclerView;
    private FJB_ListAdapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fjb);

        buttonFJBku = findViewById ( R.id.buttonFJBku );

        mRecyclerView = findViewById(R.id.recyclerView);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));

        mFJBList = new ArrayList<FJB_List>();

        createExampleList();
        buildRecyclerView();
        setButtons();

        EditText editText = findViewById(R.id.searchView);
        editText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                filter(s.toString());
            }
        });

        buttonFJBku.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(FJB.this, fjb_mylistitem.class);
                startActivity(i);
            }
        });
    }

    private void filter(String text){
        ArrayList<FJB_List> filteredList = new ArrayList<>();
        for (FJB_List list : mFJBList){
            if(list.getmNamaBarang().toLowerCase().contains(text.toLowerCase())){
                filteredList.add(list);
            }
        }
        mAdapter.filterList(filteredList);
    }

    private void removeItem(int position) {
        mFJBList.remove(position);
        mAdapter.notifyItemRemoved(position);
    }

    public void createExampleList() {
        mFJBList = new ArrayList<>();
        mFJBList.add(new FJB_List(R.drawable.fjb_nikeairjordan1, "Nike Air Jordan", "Yellow Black Color", "IDR 5.800.000"));
        mFJBList.add(new FJB_List(R.drawable.fjb_adidasrunning1, "Adidas Basic", "B&W Color, 100% Original", "IDR 1.200.000"));
        mFJBList.add(new FJB_List(R.drawable.fjb_reebokrunning1, "Reebok Running", "Unisex", "IDR 1.800.000"));

        mFJBList.add(new FJB_List(R.drawable.fjb_nikeairjordan1, "Nike Air Jordan", "Yellow Black Color", "IDR 5.800.000"));
        mFJBList.add(new FJB_List(R.drawable.fjb_adidasrunning1, "Adidas Basic", "B&W Color, 100% Original", "IDR 1.200.000"));
        mFJBList.add(new FJB_List(R.drawable.fjb_reebokrunning1, "Reebok Running", "Unisex", "IDR 1.800.000"));
    }

    private void buildRecyclerView() {
        mRecyclerView = findViewById(R.id.recyclerView);
        mRecyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(this);
        mAdapter = new FJB_ListAdapter(mFJBList);

        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setAdapter(mAdapter);

        mAdapter.setOnItemClickListener(new FJB_ListAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(int position) {
                Intent detailIntent = new Intent(FJB.this, FJB_detail.class);
                detailIntent.putExtra("Example Item", (Parcelable) mFJBList.get(position));
                startActivity(detailIntent);
            }

            @Override
            public void onDeleteClick(int position) {
                removeItem(position);
            }
//Untuk buka wa
            @Override
            public void onViewWA(int position) {
                Intent i = getPackageManager().getLaunchIntentForPackage("com.whatsapp");
                startActivity(i);
            }
        });
    }

    private Context getActivity() {
        return null;
    }

    private void setButtons() {
    }
//Menuju ke menu tambah barang
    public void launchTambahBarang(View view) {
        Intent intent = new Intent(this, FJB_Tambah_Barang.class);
        startActivity(intent);
    }

    public void launchHomeBarang(View view) {
    }

}
