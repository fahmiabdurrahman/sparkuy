package com.tim7.sparkuy;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.SearchView;
import android.widget.Toast;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.tim7.sparkuy.Model.User;

import java.util.ArrayList;

import static com.tim7.sparkuy.RecyclerViewAdapter.ALAMAT_LAPANGAN;
import static com.tim7.sparkuy.RecyclerViewAdapter.NAMA_LAPANGAN;

public class Sosial extends AppCompatActivity {

    DatabaseReference myref;
    ArrayList<String> list;
    RecyclerView recyclerView;
    EditText searchBar;
    FirebaseRecyclerOptions<User> options;
    FirebaseRecyclerAdapter<User, TemanAdapter.MyViewHolder> adapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sosial);

        myref = FirebaseDatabase.getInstance().getReference().child("User");
        recyclerView = findViewById(R.id.tampilinUser);
        searchBar = findViewById(R.id.cariTeman);

        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.addItemDecoration(new DividerItemDecoration(this, LinearLayoutManager.VERTICAL));
        searchBar.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
               filter(s.toString());
            }
        });
    }

    private void filter(String toString) {
        ArrayList<String> filteredList = new ArrayList<>();
        for (String item : list){
            if (item.toLowerCase().contains(toString.toLowerCase())){
                filteredList.add(item);
            }
        }
        TemanAdapter temanAdapter = new TemanAdapter(Sosial.this,filteredList);
        recyclerView.setAdapter(temanAdapter);
    }


    @Override
    protected void onStart() {
        super.onStart();
        if (myref != null){
            myref.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    if (dataSnapshot.exists()){
                        list = new ArrayList<>();
                        for (DataSnapshot ds : dataSnapshot.getChildren()){
                            list.add(ds.child("nama").getValue(String.class));
                        }
                        TemanAdapter temanAdapter = new TemanAdapter(Sosial.this,list);
                        recyclerView.setAdapter(temanAdapter);
                    }
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {
                    Toast.makeText(Sosial.this, databaseError.getMessage(), Toast.LENGTH_SHORT).show();
                }
            });
        }
    }
}
