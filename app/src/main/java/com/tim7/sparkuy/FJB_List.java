package com.tim7.sparkuy;

import android.os.Parcel;
import android.os.Parcelable;

public class FJB_List implements Parcelable{
    private int mImageResource;
    private String mNamaBarang;
    private String mDeskripsi;
    private String mHarga;

    public FJB_List(int ImageResource, String NamaBarang, String Deskripsi, String Harga){
        mImageResource = ImageResource;
        mNamaBarang = NamaBarang;
        mDeskripsi = Deskripsi;
        mHarga = Harga;
    }
    protected FJB_List(Parcel in) {
        mImageResource = in.readInt();
        mNamaBarang = in.readString();
        mDeskripsi = in.readString();
        mHarga = in.readString();
    }

    public static final Parcelable.Creator<FJB_List> CREATOR = new Parcelable.Creator<FJB_List>() {
        @Override
        public FJB_List createFromParcel(Parcel in) {
            return new FJB_List(in);
        }

        @Override
        public FJB_List[] newArray(int size) {
            return new FJB_List[size];
        }
    };


    public int getmImageResource() {
        return mImageResource;
    }

    public String getmNamaBarang() {
        return mNamaBarang;
    }

    public String getmDeskripsi() {
        return mDeskripsi;
    }

    public String getmHarga() {
        return mHarga;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(mImageResource);
        dest.writeString(mNamaBarang);
        dest.writeString(mDeskripsi);
        dest.writeString(mHarga);
    }
}
