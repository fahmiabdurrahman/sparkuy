package com.tim7.sparkuy;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

public class FJB_Detail_Tambah_Barang extends AppCompatActivity {

    TextView namaBarangDetail, deskripsiDetail, hargaDetail;
    ImageView imageDetail;

    private void initializeWidgets(){
        namaBarangDetail= findViewById(R.id.namaBarangDetail);
        deskripsiDetail= findViewById(R.id.deskripsiDetail);
        hargaDetail= findViewById(R.id.hargaDetail);
        imageDetail=findViewById(R.id.imageDetail);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fjb_detail_tambah_barang);

        initializeWidgets();

        //RECEIVE DATA FROM ITEMSACTIVITY VIA INTENT
        Intent i=this.getIntent();
        String mNamaBarang=i.getExtras().getString("NAME_KEY");
        String mDeskripsi=i.getExtras().getString("DESKRIPSI_KEY");
        String mHarga=i.getExtras().getString("HARGA_KEY");
        String mImageResources=i.getExtras().getString("IMAGE_KEY");

        //SET RECEIVED DATA TO TEXTVIEWS AND IMAGEVIEWS
        namaBarangDetail.setText(mNamaBarang);
        deskripsiDetail.setText(mDeskripsi);
        hargaDetail.setText(mHarga);
        Picasso.get()
                .load(mImageResources)
                //.placeholder(R.drawable.placeholder)
                .fit()
                .centerCrop()
                .into(imageDetail);

    }

}
