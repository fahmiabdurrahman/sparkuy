package com.tim7.sparkuy;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.tim7.sparkuy.Model.User;

import org.w3c.dom.Text;

import java.util.HashMap;
import java.util.Map;


public class Register extends AppCompatActivity {
    FirebaseDatabase database;
    DatabaseReference users;
    FirebaseAuth mAuth;
    EditText editNama, editEmail, editHandphone, editPassword, editAlamat;
    Button buttonRegist;
    ProgressBar progressBar;
    TextView registBack;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        database = FirebaseDatabase.getInstance();
        users = database.getReference("User");
        mAuth = FirebaseAuth.getInstance();


        editNama = findViewById(R.id.name_text);
        editEmail = findViewById(R.id.email_text);
        editHandphone = findViewById(R.id.handphone_text);
        editPassword = findViewById(R.id.password_text);

        editAlamat = findViewById(R.id.alamatField);


        progressBar = findViewById(R.id.progressBar);
        buttonRegist = (Button) findViewById(R.id.regist_button);
        buttonRegist.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final String email = editEmail.getText().toString().trim();
                final String password = editPassword.getText().toString().trim();
                final String nama = editNama.getText().toString();
                final String hp = editHandphone.getText().toString().trim();
                final String alamat = editAlamat.getText().toString().trim();

                if (TextUtils.isEmpty(nama)){
                    editNama.setError("Nama Lengkap tidak boleh kososng!");
                    return;
                }

                if (TextUtils.isEmpty(hp)){
                    editHandphone.setError("Nomer HP tidak boleh kosong!");
                    return;
                }

                if (TextUtils.isEmpty(email)) {
                    editEmail.setError("Masukkan email kamu");
                    return;
                }
                if (TextUtils.isEmpty(password)) {
                    editPassword.setError("Masukkan Password");
                    return;
                }
                if (password.length() < 5) {
                    editPassword.setError("Password min. 5 karakter");
                    return;
                }

                progressBar.setVisibility(View.VISIBLE);

                //register the user to Firebase
                mAuth.createUserWithEmailAndPassword(email, password).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        progressBar.setVisibility(View.GONE);
                        if (task.isSuccessful()) {

                            Toast.makeText(Register.this, "Akun berhasil registrasi!", Toast.LENGTH_SHORT).show();

                            User user = new User(
                                    nama,
                                    email,
                                    hp,
                                    password,
                                    alamat
                            );
                            FirebaseDatabase.getInstance().getReference("User")
                                    .child(FirebaseAuth.getInstance().getCurrentUser().getUid())
                                    .setValue(user).addOnCompleteListener(new OnCompleteListener<Void>() {
                                @Override
                                public void onComplete(@NonNull Task<Void> task) {
                                    progressBar.setVisibility(View.GONE);
                                    if (task.isSuccessful()){
                                        Toast.makeText(Register.this, "Registrasi Berhasil!", Toast.LENGTH_SHORT).show();
                                    } else {

                                    }
                                }
                            });
                            finish();
                            Toast.makeText(Register.this, "Akun sudah terdaftar!", Toast.LENGTH_SHORT).show();

                            startActivity(new Intent(getApplicationContext(), Login.class));
                        } else {
                            Toast.makeText(Register.this, "Error " + task.getException().getMessage(), Toast.LENGTH_SHORT).show();
                            progressBar.setVisibility(View.GONE);
                        }
                    }
                });
            }


        });

        registBack = findViewById(R.id.registrasi_label);
        registBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    public boolean validate(){
        boolean valid = true;

        String email = editEmail.getText().toString();
        String pass = editPassword.getText().toString();

        if (email.isEmpty()|| !Patterns.EMAIL_ADDRESS.matcher(email).matches()){
            editEmail.setError("Format Email tidak valid");
            valid = false;
        } else {
            editEmail.setError(null);
        }
        if (pass.isEmpty() || pass.length() <= 5 || pass.length() > 32){
            editPassword.setError("Harus berukuran 5 sampai 32 ");
            valid = false;
        } else {
            editPassword.setError(null);
        }
        return valid;
    }
}