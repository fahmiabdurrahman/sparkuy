package com.tim7.sparkuy;

import androidx.appcompat.app.AppCompatActivity;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.WindowManager;
import android.widget.CheckBox;

public class PengaturanActivity extends AppCompatActivity {
    public CheckBox notifUploadChekbox;
    private SharedPreferences mPreferences;
    private SharedPreferences.Editor mEditor;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pengaturan);
        notifUploadChekbox = findViewById(R.id.checkboxNotifUpload);
        mPreferences = PreferenceManager.getDefaultSharedPreferences(this);
//        mPreferences = getSharedPreferences("com.tim7.sparkuy", Context.MODE_PRIVATE);
        mEditor = mPreferences.edit();
        checkSharedPreferences();
        if (notifUploadChekbox.isChecked()){
            mEditor.putString(getString(R.string.notifikasiUpload), "True");
            mEditor.commit();
        } else {
            mEditor.putString(getString(R.string.notifikasiUpload), "False");
            mEditor.commit();

        }
        // membuat tampilan seperti pop up
        DisplayMetrics dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);

        int width = dm.widthPixels;
        int height = dm.heightPixels;

        getWindow().setLayout((int)(width), (int)(height*.7));

        WindowManager.LayoutParams params = getWindow().getAttributes();
        params.gravity = Gravity.BOTTOM;
        params.x = 0;
        params.y = 0;

        getWindow().setAttributes(params);

    }

    private void checkSharedPreferences(){
        String checkbox = mPreferences.getString(getString(R.string.notifikasiUpload), "True");

        if (checkbox.equals("True")){
            notifUploadChekbox.setChecked(true);
        } else {
            notifUploadChekbox.setChecked(false);
        }
    }
}
