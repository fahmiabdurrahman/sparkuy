package com.tim7.sparkuy;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Activity;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.TextView;
import android.widget.Toast;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.tim7.sparkuy.Model.AnggotaTim;

public class team_profile extends Activity {
    DatabaseReference ref, refUser;
    TextView tim, gk, anchor, pivot, flank1, flank2;
    String namaTim;
    String asd;
    RecyclerView recyclerView;
    DatabaseReference databaseReference;
    FirebaseRecyclerOptions<AnggotaTim> options;
    FirebaseRecyclerAdapter<AnggotaTim, anngotaRecyclerAdapter.anggotaViewHolder> adapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_team_profile);
        recyclerView = findViewById(R.id.recyclerviewtim);
        readTimData(new callback() {
            @Override
            public void getNamaTim(String timnama) {
                asd = timnama;
            }
        });




       calAdapter();

        // pop up window
        DisplayMetrics dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);

        int width = dm.widthPixels;
        int height = dm.heightPixels;

        getWindow().setLayout((int)(width*.8), (int)(height*.7));

        WindowManager.LayoutParams params = getWindow().getAttributes();
        params.gravity = Gravity.CENTER;
        params.x = 0;
        params.y = -20;

        getWindow().setAttributes(params);
    }

    private void calAdapter() {

    }

    private void readTimData(final callback callback){
        refUser = FirebaseDatabase.getInstance().getReference("User").child(FirebaseAuth.getInstance().getCurrentUser().getUid()).child("tim");
        if (refUser.getKey().equals("tidak ada") || refUser.getKey().isEmpty()){
            Toast.makeText(this, "Belum memasukin tim", Toast.LENGTH_SHORT).show();
        } else {
            refUser.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    if (!dataSnapshot.exists()){
                        Toast.makeText(team_profile.this, "Tim Tidak ada", Toast.LENGTH_SHORT).show();
                        namaTim = "Belum ada..";
                    } else {
                        namaTim = dataSnapshot.getValue().toString();
                        callback.getNamaTim(namaTim);
                        databaseReference = FirebaseDatabase.getInstance().getReference("tim").child(namaTim);
                        recyclerView.setHasFixedSize(true);
                        options = new FirebaseRecyclerOptions.Builder<AnggotaTim>()
                                .setQuery(databaseReference, AnggotaTim.class).build();
                        adapter = new FirebaseRecyclerAdapter<AnggotaTim, anngotaRecyclerAdapter.anggotaViewHolder>(options) {
                            @Override
                            protected void onBindViewHolder(@NonNull anngotaRecyclerAdapter.anggotaViewHolder holder, int position, @NonNull AnggotaTim model) {
                                holder.anggota.setText(model.getNama_anggota());
                            }

                            @NonNull
                            @Override
                            public anngotaRecyclerAdapter.anggotaViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
                                View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_anggota_tim, parent, false);
                                return new anngotaRecyclerAdapter.anggotaViewHolder(view);
                            }
                        };

                        LinearLayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
                        recyclerView.setLayoutManager(layoutManager);
                        adapter.startListening();
                        recyclerView.setAdapter(adapter);
                        tim = findViewById(R.id.teamName);
                        tim.setText(namaTim);
                    }
                }
                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {

                }
            });
        }

    }

    private interface callback{
        void getNamaTim(String timnama);
    }
}
