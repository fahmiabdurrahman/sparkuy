package com.tim7.sparkuy;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;


import android.content.Intent;
import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class MainActivity extends AppCompatActivity {


    private static final String LOG_TAG = MainActivity.class.getSimpleName();
    private long backPressedTime;
    FirebaseAuth firebaseAuth;
    TextView namaUser;
    ImageView fotoUser;
    Button buttonPelatihan;
    ProgressBar progressBar, progressBarFoto;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        BottomNavigationView bottomNavigationView = findViewById(R.id.bottom_navigation);

        bottomNavigationView.setSelectedItemId(R.id.nav_beranda);

        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                switch (menuItem.getItemId()) {
                    case R.id.nav_beranda:
                        startActivity(new Intent(getApplicationContext(), MainActivity.class));
                        return true;
                    case R.id.nav_sparing:
                        startActivity(new Intent(getApplicationContext(), pilih_lapangan_activity.class));
                        return true;
                    case R.id.nav_akun:
                        startActivity(new Intent(getApplicationContext(), akun_profile.class));
                        return true;
                }
                return false;
            }
        });
        firebaseAuth = FirebaseAuth.getInstance();
        namaUser = findViewById(R.id.namaPemain);
        fotoUser = findViewById(R.id.photoProfilePemain);
        progressBar = findViewById(R.id.progressbar_namapemain);
        progressBarFoto = findViewById(R.id.progressbarfoto);
        ImageView imageview = (ImageView) findViewById(R.id.sparing_image);
        ColorMatrix matrix = new ColorMatrix();
        matrix.setSaturation(0);

        ColorMatrixColorFilter filter = new ColorMatrixColorFilter(matrix);
        imageview.setColorFilter(filter);
        loadUserInformation();
        buttonPelatihan = findViewById(R.id.buttonPelatihan);
        buttonPelatihan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(MainActivity.this, "Loading Menu Pelatihan..", Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(MainActivity.this, BerandaPelatihan.class);
                startActivity(intent);
            }
        });
    }

    @Override
    public void onBackPressed() {

        if (backPressedTime + 2000 > System.currentTimeMillis()) {
            super.onBackPressed();
            Intent intent = new Intent(Intent.ACTION_MAIN);
            intent.setFlags(intent.FLAG_ACTIVITY_CLEAR_TOP);
            intent.addCategory(Intent.CATEGORY_HOME);
            startActivity(intent);
            finish();
            System.exit(0);
            return;
        } else {
            Toast.makeText(getBaseContext(), "Tekan kembali lagi untuk keluar", Toast.LENGTH_SHORT).show();
        }

        backPressedTime = System.currentTimeMillis();

    }

    private void loadUserInformation() {
        progressBar.setVisibility(View.VISIBLE);
        progressBarFoto.setVisibility(View.GONE);
        FirebaseUser user = firebaseAuth.getCurrentUser();
        DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference("User").child(user.getUid());
        DatabaseReference cekNama = databaseReference.child("nama");
        if (user != null) {
            if (user.getPhotoUrl() != null) {
                progressBarFoto.setVisibility(View.GONE);
                Glide.with(this)
                        .load(user.getPhotoUrl())
                        .into(fotoUser);
            }
            if (cekNama.getKey() != null) {
                cekNama.addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        progressBar.setVisibility(View.GONE);
                        namaUser.setText(dataSnapshot.getValue().toString());
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });
            }
        }
    }


    public void launchMenuFJB(View view) {
        Intent intent = new Intent(this, FJB.class);
        startActivity(intent);

    }

    public void sparing(View view) {
        Intent intent = new Intent(this, pilih_lapangan_activity.class);
        startActivity(intent);
    }

    public void tbuttonTim(View view) {
        Intent intent = new Intent(this, team_profile.class);
        startActivity(intent);
    }

    public void tbuttonriwayat(View view) {
        Intent intent = new Intent(this, riwayat_pertandingan.class);
        startActivity(intent);
    }

    public void sosialBtn(View view) {
        Intent intent = new Intent(this, PopUpSosial.class);
        startActivity(intent);
    }
}
