package com.tim7.sparkuy;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.UserProfileChangeRequest;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.IOException;

public class edit_profile extends Activity {
    private static final int CHOOSE_IMAGE = 101;
    Button editFoto, selesaiButton, kembaliButton;
    ImageView FotoProfil;
    Uri uriprofileImage;
    ProgressBar progressBar;
    String profileImageURL;
    FirebaseAuth firebaseAuth;
    EditText editName, editEmail, editPass, editHp, editAlamat;
    DatabaseReference databaseReference, cekNama, cekEmail, cekHandphone, cekPass, cekAlamat;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_edit_profile);

        DisplayMetrics dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);

        int width = dm.widthPixels;
        int height = dm.heightPixels;

        getWindow().setLayout((int) (width * .8), (int) (height - 120));

        WindowManager.LayoutParams params = getWindow().getAttributes();
        params.gravity = Gravity.CENTER;
        params.x = 0;
        params.y = -20;

        getWindow().setAttributes(params);

        firebaseAuth = FirebaseAuth.getInstance();
        editName = findViewById(R.id.editNameField);
        editEmail = findViewById(R.id.editEmailField);
        editEmail.setEnabled(false);
        editPass = findViewById(R.id.editPasswordField);
        editHp = findViewById(R.id.editHandphoneField);
        editAlamat = findViewById(R.id.editAddressField);
        FotoProfil = findViewById(R.id.editPhotoImage);
        editFoto = findViewById(R.id.editPhotoButton);
        selesaiButton = findViewById(R.id.selesaiButton);
        progressBar = findViewById(R.id.progressbar_editprofil);
        editFoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editProfilePhoto();
            }
        });
        selesaiButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                saveUserInformation();
            }
        });
        kembaliButton = findViewById(R.id.kembaliButton);
        kembaliButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        loadUserInformation();
    }

    private void loadUserInformation() {
        FirebaseUser user = firebaseAuth.getCurrentUser();
        databaseReference = FirebaseDatabase.getInstance().getReference("User").child(user.getUid());
        cekNama = databaseReference.child("nama");
        cekEmail = databaseReference.child("email");
        cekHandphone = databaseReference.child("handphone");
        cekPass = databaseReference.child("password");
        cekAlamat = databaseReference.child("alamat");
        if (user != null) {
            if (user.getPhotoUrl() != null) {
                Glide.with(this)
                        .load(user.getPhotoUrl())
                        .into(FotoProfil);
            }
            if (cekNama.getKey() != null) {
                cekNama.addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        editName.setText(dataSnapshot.getValue().toString());
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });
            }
            if (cekEmail.getKey() != null){
                cekEmail.addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        editEmail.setText(dataSnapshot.getValue().toString());
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });
            }
            if (cekPass.getKey() != null){
                cekPass.addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        editPass.setText(dataSnapshot.getValue().toString());
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });
            }
            if (cekHandphone.getKey() != null){
                cekHandphone.addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        editHp.setText(dataSnapshot.getValue().toString());
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });
            }
            if (cekAlamat.getKey() != null){
                cekAlamat.addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        editAlamat.setText(dataSnapshot.getValue().toString());
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });
            }
        }
    }

    private void saveUserInformation() {
        String namaLengkap = editName.getText().toString();
        if (namaLengkap.isEmpty()) {
            editName.setError("Namaa");
            editName.requestFocus();
            return;
        }
        FirebaseUser user = firebaseAuth.getCurrentUser();
        if (profileImageURL != null) {
            UserProfileChangeRequest profile = new UserProfileChangeRequest.Builder()
                    .setPhotoUri(Uri.parse(profileImageURL)).build();
            user.updateProfile(profile)
                    .addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            if (task.isSuccessful()) {
                                Toast.makeText(edit_profile.this, "Profile diubah!", Toast.LENGTH_SHORT).show();
                            }
                        }
                    });
        }
        if (user != null){
            cekNama.setValue(editName.getText().toString());
            cekPass.setValue(editPass.getText().toString());
            cekHandphone.setValue(editHp.getText().toString());
            cekAlamat.setValue(editAlamat.getText().toString());
            Toast.makeText(this, "Sukses Mengupdate Profile!", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == CHOOSE_IMAGE && resultCode == RESULT_OK && data != null && data.getData() != null) {
            uriprofileImage = data.getData();
            try {
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), uriprofileImage);
                FotoProfil.setImageBitmap(bitmap);
                uploadImageToFirebaseStorage();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private void uploadImageToFirebaseStorage() {
        FirebaseUser user = firebaseAuth.getCurrentUser();
        StorageReference profileImageReference = FirebaseStorage.getInstance().getReference("userProfile/" + user.getUid()+ " .jpg");
        if (uriprofileImage != null) {
            progressBar.setVisibility(View.VISIBLE);
            profileImageReference.putFile(uriprofileImage).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                    if (taskSnapshot.getMetadata() != null) {
                        if (taskSnapshot.getMetadata().getReference() != null) {
                            Task<Uri> result = taskSnapshot.getStorage().getDownloadUrl();
                            result.addOnSuccessListener(new OnSuccessListener<Uri>() {
                                @Override
                                public void onSuccess(Uri uri) {
                                    progressBar.setVisibility(View.GONE);
                                    profileImageURL = uri.toString();

                                }
                            });
                        }
                    }

                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    progressBar.setVisibility(View.GONE);
                    Toast.makeText(edit_profile.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                }
            });
        }
    }

    public void editProfilePhoto() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select Foto Profil"), CHOOSE_IMAGE);
    }
}
