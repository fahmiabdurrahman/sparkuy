package com.tim7.sparkuy;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.tim7.sparkuy.Model.Aktivitas;

import java.util.ArrayList;

public class TambahAktivitas extends AppCompatActivity {

    DatabaseReference reff;
    EditText editDetailAktivitas,edit_aktivitas;
    Button tambahAktivitas;
    ArrayList<String> namaPemain;
    ArrayList<String> namaAktivitas;
    ArrayList<String> detailAktivitas;
    String data;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tambah_aktivitas);

        editDetailAktivitas = findViewById(R.id.editDetailAktivitas);
        edit_aktivitas =findViewById(R.id.edit_aktivitas);
        tambahAktivitas = findViewById(R.id.tambahAktivitas);

        DisplayMetrics dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);

        int width = dm.widthPixels;
        int height = dm.heightPixels;

        getWindow().setLayout((int)(width*.9), (int)(height*.9));

        WindowManager.LayoutParams params = getWindow().getAttributes();
        params.gravity = Gravity.CENTER;
        params.x = 0;
        params.y = -20;

        getWindow().setAttributes(params);
    }

    public void addActivity(View view) {

        FirebaseDatabase database = FirebaseDatabase.getInstance();
        final DatabaseReference myRef = database.getReference("Aktivitas");
        final DatabaseReference individuRef = database.getReference("Aktivitas Individu").child(FirebaseAuth.getInstance().getCurrentUser().getUid());

        final String detailAktivitas = editDetailAktivitas.getText().toString();
        final String  aktivitas= edit_aktivitas.getText().toString();
        if (TextUtils.isEmpty(aktivitas)) {

            Toast.makeText(this, "Aktivitas tidak boleh kosong", Toast.LENGTH_SHORT).show();

        }else{
            DatabaseReference pushKey = myRef.push();
            final String key = pushKey.getKey();
            DatabaseReference userRef =  FirebaseDatabase.getInstance().getReference().child("User").child(FirebaseAuth.getInstance().getCurrentUser().getUid()).child("nama");
            userRef.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    String userName = dataSnapshot.getValue().toString();
                    Aktivitas aktivitas1 = new Aktivitas(aktivitas, detailAktivitas,userName);
                    myRef.child(key).setValue(aktivitas1);
                    individuRef.child(key).setValue(aktivitas1);
                    Toast.makeText(TambahAktivitas.this, "Aktivitas berhasil ditambahkan", Toast.LENGTH_SHORT).show();
                    onBackPressed();
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {

                }
            });

    }

    }
}
