package com.tim7.sparkuy;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.Random;

import static com.tim7.sparkuy.DetailPertandinganSolo.JADWAL_PERTANDINGAN;
import static com.tim7.sparkuy.DetailPertandinganSolo.POSISI;
import static com.tim7.sparkuy.DetailPertandinganSolo.TIM_PILIHAN;
import static com.tim7.sparkuy.RecyclerViewAdapter.NAMA_LAPANGAN;

public class DetailBooking extends AppCompatActivity {
    FirebaseAuth firebaseAuth;
    DatabaseReference lapanganRef;
    Button cekStatus, uploadBukti;
    TextView namaPemain, tanggalBermain, namaLapangan, alamatLapangan, biayaTagihanText, kodeUnikTextView, totalBiayaText, biayaAdminText;
    String bermainSebagai = ""; // ditambahkan ke biayaTagihanText
    int biayaTagihan = 0;
    int biayaAdmin = 10000;
    int kodeUnik = 0;
    int totaltagihan = 0;
    int orderid = 0;
    String tagihanText;
    String nama_pemain;
    public static final String NAMA_PEMAIN = "com.tim7.sparkuy.nama_pemain";
    public static final String ORDER = "com.tim7.sparkuy.order";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_booking);
        firebaseAuth = FirebaseAuth.getInstance();
        Intent intent = getIntent();
        final String nama_lapangan = intent.getStringExtra(NAMA_LAPANGAN);
        final String jadwal_pertandingan_solo = intent.getStringExtra(JadwalPertandingan.JADWAL_PERTANDINGAN);
        final String biaya = intent.getStringExtra(DetailPertandinganSolo.BIAYA);
        final String posisi = intent.getStringExtra(POSISI);
        final String timPilihan = intent.getStringExtra(TIM_PILIHAN);
        biayaTagihan = Integer.parseInt(biaya);
         lapanganRef = FirebaseDatabase.getInstance().getReference().child("lapangan").child(nama_lapangan);
        uploadBukti = findViewById(R.id.buttonUploadBuktiPembayaran);
        namaPemain = findViewById(R.id.namaPemain);
        tanggalBermain = findViewById(R.id.tanggalBermainDetailBooking);
        namaLapangan = findViewById(R.id.tempatBertanding);
        alamatLapangan = findViewById(R.id.alamatTempatBertanding);
        biayaTagihanText = findViewById(R.id.BermainSebagai);
        biayaAdminText = findViewById(R.id.biayaAdmin);
        kodeUnikTextView = findViewById(R.id.kodeUnikLabel);
        totalBiayaText = findViewById(R.id.totalBiaya);
        loadDetailBookingInformation();
        tanggalBermain.setText(jadwal_pertandingan_solo);
        namaLapangan.setText(nama_lapangan);
        if (posisi.equals("NULL")){
            tagihanText = "Bermain di "+timPilihan+" : Rp."+biayaTagihan;

        } else {
            tagihanText = "Bermain di "+timPilihan+" sebagai " + posisi +" : Rp."+biayaTagihan;
        }
        biayaTagihanText.setText(tagihanText);
         kodeUnik = randomKodeUnik(100, 500);
        String kodeUnikText = "Kode Unik : Rp. "+kodeUnik;
        kodeUnikTextView.setText(kodeUnikText);
        totaltagihan = biayaTagihan + biayaAdmin + kodeUnik;
        String textTotal = "TOTAL : Rp. "+totaltagihan;
        totalBiayaText.setText(textTotal);
        uploadBukti.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                orderid++;
                DatabaseReference orderRef = FirebaseDatabase.getInstance().getReference("riwayat_pesanan").child("order_counter");
                orderRef.setValue(orderid);
                Intent intent = new Intent(DetailBooking.this, upload_bukti_pembayaran.class);
                intent.putExtra(POSISI, posisi);
                intent.putExtra(TIM_PILIHAN, timPilihan);
                intent.putExtra(NAMA_PEMAIN, nama_pemain);
                intent.putExtra(ORDER, orderid);
                intent.putExtra(NAMA_LAPANGAN, nama_lapangan);
                intent.putExtra(JADWAL_PERTANDINGAN, jadwal_pertandingan_solo);
                startActivity(intent);
            }
        });
        // membuat tampilan seperti pop up
        DisplayMetrics dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);

        int width = dm.widthPixels;
        int height = dm.heightPixels;

        getWindow().setLayout((int)(width), (int)(height*.7));

        WindowManager.LayoutParams params = getWindow().getAttributes();
        params.gravity = Gravity.BOTTOM;
        params.x = 0;
        params.y = 0;

        getWindow().setAttributes(params);
    }

    public static int randomKodeUnik(int min, int max){
        Random random = new Random();
        return random.nextInt((max-min) +1) + min;
    }

    private void loadDetailBookingInformation(){
        FirebaseUser user = firebaseAuth.getCurrentUser();
        DatabaseReference namaPemainRef = FirebaseDatabase.getInstance().getReference("User").child(user.getUid()).child("nama");
        namaPemainRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                namaPemain.setText(dataSnapshot.getValue().toString());
                nama_pemain = namaPemain.getText().toString();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
        lapanganRef.child("alamat").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                alamatLapangan.setText(dataSnapshot.getValue().toString());
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
        DatabaseReference orderRef = FirebaseDatabase.getInstance().getReference("riwayat_pesanan").child("order_counter");
        orderRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                orderid = Integer.parseInt(dataSnapshot.getValue().toString());
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

}
