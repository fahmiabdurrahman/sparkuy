package com.tim7.sparkuy;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.ContentResolver;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.webkit.MimeTypeMap;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import android.os.Handler;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.StorageTask;
import com.google.firebase.storage.UploadTask;
import com.squareup.picasso.Picasso;
import com.tim7.sparkuy.Model.MyFJB;

import java.util.HashMap;


public class FJB_Tambah_Barang extends AppCompatActivity {
    private static final int PICK_IMAGE_REQUEST = 1;

    private Button mButtonChooseImage, mButtonUpload, mShowUploads;
    private EditText mEditTextFileName, mEditTextNamaBarang, mEditTextDeskripsi, mEditTextHarga, mEditTextNomorWA;
    private ImageView mImageView;
    private ProgressBar mProgressBar;
    HashMap<String, String> hashMap;
    MyFJB fjb;
    long valueId = 0;


    private Uri mImageUri;

    private StorageReference mStorageRef;
    private DatabaseReference mDatabaseRef;

    private StorageTask mUploadTask;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fjb_tambah_barang);

        fjb = new MyFJB();
        //untuk show list


        mButtonChooseImage = findViewById(R.id.buttonChooseImage);
        mButtonUpload = findViewById(R.id.buttonUplodBarang);
        mShowUploads = findViewById(R.id.buttonMyFJB);
        mEditTextFileName = findViewById(R.id.editText_namaGambar);
        mEditTextNamaBarang = findViewById(R.id.editText_namaBarang);
        mEditTextDeskripsi = findViewById(R.id.editText_deskripsi);
        mEditTextHarga = findViewById(R.id.editText_harga);
        mEditTextNomorWA = findViewById(R.id.editText_nomorwa);
        mImageView = findViewById(R.id.imageViewLatarFJB);
        mProgressBar = findViewById(R.id.progress_bar);

        mStorageRef = FirebaseStorage.getInstance().getReference("fjb_uploads");
        mDatabaseRef = FirebaseDatabase.getInstance().getReference().child("fjb_uploads");

        mButtonChooseImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openFileChooser();
            }
        });
        mButtonUpload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mUploadTask != null && mUploadTask.isInProgress()) {
                    Toast.makeText(FJB_Tambah_Barang.this, "Proses upload...", Toast.LENGTH_SHORT).show();
                } else {
                    uploadFile();
                }

                fjb.setNama_barang(mEditTextNamaBarang.getText().toString().trim());
                fjb.setDeskripsi_barang(mEditTextDeskripsi.getText().toString().trim());
                fjb.setNama_filebarang(mEditTextFileName.getText().toString());
                fjb.setHarga_barang(mEditTextHarga.getText().toString().trim());
                fjb.setNomor_wa(mEditTextNomorWA.getText().toString().trim());
                mDatabaseRef.push().setValue(fjb);
                startActivity(new Intent(getApplicationContext(), FJB_Tambah_Barang.class));
            }
        });
        mShowUploads.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
    }

    private void openFileChooser() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(intent, PICK_IMAGE_REQUEST);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PICK_IMAGE_REQUEST && resultCode == RESULT_OK && data != null && data.getData() != null) {
            final Uri mImageUri = data.getData();


            Picasso.get().load(mImageUri).into(mImageView);

        }}
    private String getFileExtension(Uri uri) {
        ContentResolver cR = getContentResolver();
        MimeTypeMap mime = MimeTypeMap.getSingleton();
        return mime.getExtensionFromMimeType(cR.getType(uri));
    }

    private void uploadFile() {
        if (mImageUri != null) {
            StorageReference fileReference = mStorageRef.child(System.currentTimeMillis()
                    + "." + getFileExtension(mImageUri));
            mProgressBar.setVisibility(View.VISIBLE);
            mProgressBar.setIndeterminate(true);

            mUploadTask = fileReference.putFile(mImageUri)
                    .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                            Handler handler = new Handler();
                            handler.postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    mProgressBar.setVisibility(View.VISIBLE);
                                    mProgressBar.setIndeterminate(false);
                                    mProgressBar.setProgress(0);
                                }
                            }, 500);

                            Toast.makeText(FJB_Tambah_Barang.this, "Upload Berhasil!", Toast.LENGTH_LONG).show();
                            FJB_Teacher upload = new FJB_Teacher(mEditTextNamaBarang.getText().toString().trim(),
                                    taskSnapshot.getUploadSessionUri().toString());

                            String uploadId = mDatabaseRef.push().getKey();
                            mDatabaseRef.child(uploadId).setValue(upload);
                            mProgressBar.setVisibility(View.INVISIBLE);
                            openImagesActivity ();
                        }
                    })
                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            Toast.makeText(FJB_Tambah_Barang.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    })
                    .addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
                            double progress = (100.0 * taskSnapshot.getBytesTransferred() / taskSnapshot.getTotalByteCount());

                            mProgressBar.setProgress((int) progress);
                        }
                    });
        } else {
            Toast.makeText(this, "Berhasil Upload", Toast.LENGTH_SHORT).show();
        }
    }



    private void openImagesActivity(){
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }


    public void showMessage(String title, String Message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setCancelable(true);
        builder.setTitle(title);
        builder.setMessage(Message);
        builder.show();
    }


    public void menuAwal(View view) {
        Intent intent = new Intent(this, FJB.class);
        startActivity(intent);
    }
}


