package com.tim7.sparkuy;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.textfield.TextInputEditText;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.tim7.sparkuy.Model.Masukan;


public class pb_masukan extends AppCompatActivity {
    TextInputEditText inputmasukan;
    Button btnsave;
    DatabaseReference reff;
    Masukan masukan;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.pb_masukan);
        btnsave = (Button) findViewById(R.id.pb_masukan_button_kirim);
        inputmasukan = (TextInputEditText) findViewById(R.id.textbox_masukan);
        masukan = new Masukan();
        reff = FirebaseDatabase.getInstance().getReference().child("Masukan");
        btnsave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            masukan.setMasukan(inputmasukan.getText().toString().trim());
            reff.push().setValue(masukan);
            Toast.makeText(pb_masukan.this,"Masukan Terkirim",Toast.LENGTH_LONG).show();
            startActivity(new Intent(getApplicationContext(), pusat_bantuan.class));
            }
        });

    }
}
