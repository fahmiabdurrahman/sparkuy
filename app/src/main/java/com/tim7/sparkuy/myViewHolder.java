package com.tim7.sparkuy;

import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

class myViewHolder extends RecyclerView.ViewHolder {

    TextView aktivitasLabel, detailAktivitasLabel;

    public myViewHolder(@NonNull View itemView) {
        super(itemView);

        aktivitasLabel=itemView.findViewById(R.id.aktivitasLabel);
        detailAktivitasLabel=itemView.findViewById(R.id.detailAktivitasLabel);
    }
}
