package com.tim7.sparkuy;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.tim7.sparkuy.Model.Aktivitas;

import java.util.ArrayList;


/**
 * A simple {@link Fragment} subclass.
 */
public class BerandaPelatihanFrag extends Fragment {

    EditText aktivitasLabel, detailAktivitasLabel;

    DatabaseReference myref;
    ArrayList<String> judulAktivitas;
    ArrayList<String> detailAktivitas;
    RecyclerView listAktivitas;
    Button tambahAktivitas, editAktivitas;
    public BerandaPelatihanFrag() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        //define recycler view
        myref = FirebaseDatabase.getInstance().getReference().child("Aktivitas Individu").child(FirebaseAuth.getInstance().getCurrentUser().getUid());
        myref.keepSynced(true);

         View view = inflater.inflate(R.layout.fragment_beranda_pelatihan, container, false);
        tambahAktivitas = view.findViewById(R.id.addAktivitasButton);
        tambahAktivitas.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), TambahAktivitas.class);
                startActivity(intent);
            }
        });

        LinearLayoutManager llm = new LinearLayoutManager(getContext());
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        listAktivitas = (RecyclerView) view.findViewById(R.id.recyclerAktivitas);
        listAktivitas.setHasFixedSize(true);
        listAktivitas.setLayoutManager(llm);
        loadData();
        // Inflate the layout for this fragment
        return view;
    }
    private void loadData(){
        myref.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()){
                    judulAktivitas = new ArrayList<>();
                    detailAktivitas = new ArrayList<>();
                    for (DataSnapshot ds : dataSnapshot.getChildren()){
                        judulAktivitas.add(ds.child("aktivitas").getValue(String.class));
                        detailAktivitas.add(ds.child("detailAktivitas").getValue(String.class));
                    }
                    BerandaAktivitasAdapter berandaAktivitasAdapter = new BerandaAktivitasAdapter(getActivity(),judulAktivitas,detailAktivitas);
                    listAktivitas.setAdapter(berandaAktivitasAdapter);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }
}
