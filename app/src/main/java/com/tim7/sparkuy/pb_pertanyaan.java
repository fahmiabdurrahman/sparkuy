package com.tim7.sparkuy;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.google.android.material.textfield.TextInputEditText;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.tim7.sparkuy.Model.AjuanPertanyaan;
import com.tim7.sparkuy.Model.Masukan;


public class pb_pertanyaan extends AppCompatActivity {
    private TextInputEditText inputsubject;
    private TextInputEditText inputpertanyaan;
    Button btnsave;
    DatabaseReference reff;
    AjuanPertanyaan pertanyaan;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.pb_pertanyaan);
        btnsave = (Button) findViewById(R.id.pb_pertanyaan_button_kirim);
        inputsubject = (TextInputEditText) findViewById(R.id.inputSubject);
        inputpertanyaan = (TextInputEditText) findViewById(R.id.inputPertanyaan);
        pertanyaan = new AjuanPertanyaan();
        reff = FirebaseDatabase.getInstance().getReference().child("AjuanPertanyaan");
        btnsave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pertanyaan.setSubject(inputsubject.getText().toString().trim());
                pertanyaan.setPertanyaan(inputpertanyaan.getText().toString().trim());
                reff.push().setValue(pertanyaan);
                Toast.makeText(pb_pertanyaan.this,"Pertanyaan Terkirim",Toast.LENGTH_LONG).show();
                startActivity(new Intent(getApplicationContext(), pusat_bantuan.class));
                finish();
            }
        });

    }
}