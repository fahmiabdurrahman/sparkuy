package com.tim7.sparkuy;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.NotificationCompat;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;
import android.webkit.MimeTypeMap;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.StorageTask;
import com.google.firebase.storage.UploadTask;

import static com.tim7.sparkuy.DetailBooking.NAMA_PEMAIN;
import static com.tim7.sparkuy.DetailBooking.ORDER;
import static com.tim7.sparkuy.DetailPertandinganSolo.JADWAL_PERTANDINGAN;
import static com.tim7.sparkuy.DetailPertandinganSolo.POSISI;
import static com.tim7.sparkuy.DetailPertandinganSolo.TIM_PILIHAN;
import static com.tim7.sparkuy.RecyclerViewAdapter.NAMA_LAPANGAN;
import com.tim7.sparkuy.PengaturanActivity;

public class upload_bukti_pembayaran extends AppCompatActivity {
    DatabaseReference ref, timRef;
    DatabaseReference waitingRef;
    private static final int PICK_IMAGE_REQUEST = 1;

    private Button mButtonChooseImage;
    private Button mButtonUpload;
    private ImageView mImageView;
    private ProgressBar mProgressBar;
    private static final String ACTION_UPDATE_NOKTIFICATION = "com.example.notifyme.ACTION_UPDATE_NOTIFICATION";
    private static final String PRIMARY_CHANNEL_ID = "primary_notification_channel";
    private NotificationManager mNotifyManager;
    private static final int NOTIFICATION_ID = 0;
    private Uri mImageUri;
    private StorageTask mUploadTask;
    private StorageReference mStorageRef;
    private DatabaseReference mDatabaseRef;
    int waitingPlayerCounter = 0;
    int order = 0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_upload_bukti_pembayaran);
        Intent intent = getIntent();
        final String posisi = intent.getStringExtra(POSISI);
        final String tim = intent.getStringExtra(TIM_PILIHAN);
        final String nama_pemain = intent.getStringExtra(NAMA_PEMAIN);
        order = intent.getIntExtra(ORDER, 0);
        final String nama_lapangan = intent.getStringExtra(NAMA_LAPANGAN);
        final String jadwal_pertandingan = intent.getStringExtra(JADWAL_PERTANDINGAN);
        mButtonChooseImage = findViewById(R.id.buttonPilihGambar);
        mButtonUpload = findViewById(R.id.buttonUpload);
        mImageView = findViewById(R.id.imageViewUploadBukti);
        mProgressBar = findViewById(R.id.progressbarHorizontal);

        mStorageRef = FirebaseStorage.getInstance().getReference("riwayat_pesanan").child(FirebaseAuth.getInstance().getCurrentUser().getUid()).child(String.valueOf(order));
        mDatabaseRef = FirebaseDatabase.getInstance().getReference("riwayat_pesanan").child(FirebaseAuth.getInstance().getCurrentUser().getUid()).child(String.valueOf(order)).child("foto");
        ref = FirebaseDatabase.getInstance().getReference("detail_pertandingan_lapangan").child(nama_lapangan).child("waiting_match");
        timRef = FirebaseDatabase.getInstance().getReference("detail_pertandingan_lapangan").child(nama_lapangan).child("booked_match").child(jadwal_pertandingan).child("Dipesan_oleh");
        waitingRef = ref.child("membutuhkan_pemain");
        waitingRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                waitingPlayerCounter =Integer.parseInt(dataSnapshot.getValue().toString());
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
        mButtonChooseImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openFileChooser();
            }
        });

        mButtonUpload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mUploadTask != null && mUploadTask.isInProgress()){
                    Toast.makeText(upload_bukti_pembayaran.this, "Mohon bersabar, upload sedang proses", Toast.LENGTH_SHORT).show();
                } else {
                uploadFile();
                if (!jadwal_pertandingan.isEmpty()) {
                    waitingPlayerCounter--;
                    if (waitingPlayerCounter < 0){
                        waitingPlayerCounter = 0;
                    }
                    waitingRef.setValue(waitingPlayerCounter);
                    DatabaseReference pemainRef = ref.child(tim).child(posisi);
                    pemainRef.setValue(nama_pemain);
                } else {
                    timRef.setValue(nama_pemain);
                }


            }}
        });
        // membuat tampilan seperti pop up
        DisplayMetrics dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);

        int width = dm.widthPixels;
        int height = dm.heightPixels;

        getWindow().setLayout((int)(width), (int)(height*.7));

        WindowManager.LayoutParams params = getWindow().getAttributes();
        params.gravity = Gravity.BOTTOM;
        params.x = 0;
        params.y = 0;

        getWindow().setAttributes(params);
        createNotificaitonChannel();
    }

    private String getFileExtension(Uri uri){
        ContentResolver contentResolver = getContentResolver();
        MimeTypeMap mime = MimeTypeMap.getSingleton();
        return mime.getExtensionFromMimeType(contentResolver.getType(uri));
    }

    private void uploadFile() {
        if (mImageUri != null){
            StorageReference fileReference = mStorageRef.child(System.currentTimeMillis() + "." + getFileExtension(mImageUri));
            mUploadTask = fileReference.putFile(mImageUri)
                    .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                            Handler handler = new Handler();
                            handler.postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    mProgressBar.setProgress(0);
                                }
                            }, 3000);
                            Toast.makeText(upload_bukti_pembayaran.this, "Upload Bukti Berhasil!", Toast.LENGTH_SHORT).show();
                            PengaturanActivity p = new PengaturanActivity();
                            CheckBox notifUploadChekbox = findViewById(R.id.checkboxNotifUpload);
                                sendNotification();
                            Intent intent = new Intent(upload_bukti_pembayaran.this, StatusPembayaran.class);
                            intent.putExtra(ORDER, order);
                            startActivity(intent);
                            Task<Uri> result = taskSnapshot.getStorage().getDownloadUrl();
                            result.addOnSuccessListener(new OnSuccessListener<Uri>() {
                                @Override
                                public void onSuccess(Uri uri) {
                                    final String downloadUrl = uri.toString();
                                    mDatabaseRef.setValue(downloadUrl);

                                }
                            });
                        }
                    })
                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            Toast.makeText(upload_bukti_pembayaran.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    })
                    .addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onProgress(@NonNull UploadTask.TaskSnapshot taskSnapshot) {
                            double progress = (100.0 * taskSnapshot.getBytesTransferred() / taskSnapshot.getTotalByteCount());
                            mProgressBar.setProgress((int)progress);
                        }
                    });
        } else {
            Toast.makeText(this, "Tidak ada file yang terplih", Toast.LENGTH_SHORT).show();
        }
    }

    private void sendNotification() {
        Intent updateIntent = new Intent(ACTION_UPDATE_NOKTIFICATION);
        getNotificationBuilder();
        NotificationCompat.Builder notifyBuilder = getNotificationBuilder();
        mNotifyManager.notify(NOTIFICATION_ID, notifyBuilder.build());
    }

    private void openFileChooser() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(intent, PICK_IMAGE_REQUEST);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PICK_IMAGE_REQUEST && resultCode == RESULT_OK && data != null && data.getData() != null){
            mImageUri = data.getData();

            Glide.with(this).load(mImageUri).into(mImageView);
        }
    }
    private NotificationCompat.Builder getNotificationBuilder(){
        Intent notificationIntent = new Intent(this, MainActivity.class);
        PendingIntent notificationPendingIntent = PendingIntent.getActivity(this, NOTIFICATION_ID, notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        NotificationCompat.Builder notifyBuilder =  new NotificationCompat.Builder(this, PRIMARY_CHANNEL_ID)
                .setContentTitle("Bukti Pembayaran berhasil di upload!")
                .setContentText("Jangan lupa junjung sportivitas!")
                .setSmallIcon(R.drawable.sparkuy_logo)
                .setContentIntent(notificationPendingIntent)
                .setAutoCancel(true)
                .setPriority(NotificationCompat.PRIORITY_HIGH)
                .setDefaults(NotificationCompat.DEFAULT_ALL);

        return notifyBuilder;
    }
    public void createNotificaitonChannel(){
        mNotifyManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O){
            NotificationChannel notificationChannel = new NotificationChannel(PRIMARY_CHANNEL_ID, "Mascot Notification", NotificationManager.IMPORTANCE_HIGH);
            notificationChannel.enableLights(true);
            notificationChannel.setLightColor(Color.RED);
            notificationChannel.enableVibration(true);
            notificationChannel.setDescription("Notification from Mascot");
            mNotifyManager.createNotificationChannel(notificationChannel);
        }
    }
}
