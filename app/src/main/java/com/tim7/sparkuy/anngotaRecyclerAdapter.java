package com.tim7.sparkuy;

import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class anngotaRecyclerAdapter extends RecyclerView.Adapter<anngotaRecyclerAdapter.anggotaViewHolder> {
    @NonNull
    @Override
    public anngotaRecyclerAdapter.anggotaViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return null;
    }

    @Override
    public void onBindViewHolder(@NonNull anngotaRecyclerAdapter.anggotaViewHolder holder, int position) {

    }

    @Override
    public int getItemCount() {
        return 0;
    }

    public static class anggotaViewHolder extends RecyclerView.ViewHolder {
        public final TextView anggota;
        public anggotaViewHolder(@NonNull View itemView) {
            super(itemView);
            anggota = itemView.findViewById(R.id.anggotaText);
        }
    }
}
