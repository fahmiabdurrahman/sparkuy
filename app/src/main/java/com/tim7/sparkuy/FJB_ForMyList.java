package com.tim7.sparkuy;

import android.net.Uri;

public class FJB_ForMyList {
    private String deskripsi_barang, harga_barang, nama_barang, nama_filebarang, nomor_wa, foto;
    public FJB_ForMyList(){

    }

    public FJB_ForMyList(String deskripsi_barang, String harga_barang, String nama_barang, String nama_filebarang, String nomor_wa, String foto) {
        this.deskripsi_barang = deskripsi_barang;
        this.harga_barang = harga_barang;
        this.nama_barang = nama_barang;
        this.nama_filebarang = nama_filebarang;
        this.nomor_wa = nomor_wa;
        this.foto = foto;
    }

    @Override
    public String toString() {
        return "FJB_ForMyList{" +
                "deskripsi_barang='" + deskripsi_barang + '\'' +
                ", harga_barang='" + harga_barang + '\'' +
                ", nama_barang='" + nama_barang + '\'' +
                ", nama_filebarang='" + nama_filebarang + '\'' +
                ", nomor_wa='" + nomor_wa + '\'' +
                ", foto='" + foto + '\'' +
                '}';
    }

    public String getDeskripsi_barang() {
        return deskripsi_barang;
    }

    public void setDeskripsi_barang(String deskripsi_barang) {
        this.deskripsi_barang = deskripsi_barang;
    }

    public String getHarga_barang() {
        return harga_barang;
    }

    public void setHarga_barang(String harga_barang) {
        this.harga_barang = harga_barang;
    }

    public String getNama_barang() {
        return nama_barang;
    }

    public void setNama_barang(String nama_barang) {
        this.nama_barang = nama_barang;
    }

    public String getNama_filebarang() {
        return nama_filebarang;
    }

    public void setNama_filebarang(String nama_filebarang) {
        this.nama_filebarang = nama_filebarang;
    }

    public String getNomor_wa() {
        return nomor_wa;
    }

    public void setNomor_wa(String nomor_wa) {
        this.nomor_wa = nomor_wa;
    }

    public Uri getFoto() {
        return Uri.parse(foto);
    }

    public void setFoto(String foto) {
        this.foto = foto;
    }
}
