package com.tim7.sparkuy;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

import static com.tim7.sparkuy.DetailBooking.ORDER;

public class StatusPembayaran extends AppCompatActivity {
    Button kembaliHome;
    TextView orderText;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_status_pembayaran);
        Intent intent = getIntent();
        final int order = intent.getIntExtra(ORDER, 0);
        orderText = findViewById(R.id.orderId);
        String textOrder = "ORDER ID : "+order;
        orderText.setText(textOrder);
        kembaliHome = findViewById(R.id.buttonKembaliMenuUtama);
        kembaliHome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(StatusPembayaran.this, MainActivity.class);
                startActivity(intent);
            }
        });
        // membuat tampilan seperti pop up
        DisplayMetrics dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);

        int width = dm.widthPixels;
        int height = dm.heightPixels;

        getWindow().setLayout((int)(width), (int)(height*.7));

        WindowManager.LayoutParams params = getWindow().getAttributes();
        params.gravity = Gravity.BOTTOM;
        params.x = 0;
        params.y = 0;

        getWindow().setAttributes(params);
    }
}
