package com.tim7.sparkuy;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ShareCompat;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;


/**
 * A simple {@link Fragment} subclass.
 */
public class NotifikasiFrag extends Fragment {
    private Button shareButton;
    public NotifikasiFrag() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_notifikasi, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        shareButton = view.findViewById(R.id.buttonnotifikasi);
        shareButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //share text
                String yes = "Saya sudah minum air 12x dalam sehari! Ayo jaga kesehatan!";
                String mimeType = "text/plain";
                ShareCompat.IntentBuilder.from((BerandaPelatihan) getActivity()).setType(mimeType).setChooserTitle("Share using").setText(yes).startChooser();
            }
        });
    }
}