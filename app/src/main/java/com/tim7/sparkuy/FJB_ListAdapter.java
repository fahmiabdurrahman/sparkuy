package com.tim7.sparkuy;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.tim7.sparkuy.FJB_List;
import com.tim7.sparkuy.R;

import java.util.ArrayList;
import java.util.List;

public class FJB_ListAdapter extends RecyclerView.Adapter<FJB_ListAdapter.FJBExampleViewHolder> implements Filterable {
    private ArrayList<FJB_List> mFJBList;
    //Untuk FIlter :
    private ArrayList<FJB_List> mFJBListFull;
    private OnItemClickListener mListener;
    private Context mContext;
    PackageManager packageManager;

    public FJB_ListAdapter(Context context, ArrayList<FJB_List> FJBList){
        mContext = context;
        mFJBList = FJBList;
    }

    public interface OnItemClickListener{
        void onItemClick(int position);
        void onDeleteClick(int position);
        void onViewWA (int position);
    }

    public void setOnItemClickListener(OnItemClickListener listener){
        mListener = listener;
    }

    public class FJBExampleViewHolder extends RecyclerView.ViewHolder{
        public ImageView mImageView;
        public TextView mNamaBarang;
        public TextView mDeskripsi;
        public TextView mHarga;
        public ImageView mImageDelete;
        public RelativeLayout parentLayout;
        public Button mButtonBeli;

        public FJBExampleViewHolder(View itemView, OnItemClickListener mListener) {
            super(itemView);
            mImageView = itemView.findViewById(R.id.imageBarang);
            mNamaBarang = itemView.findViewById(R.id.textViewTitle);
            mDeskripsi = itemView.findViewById(R.id.textViewDesc);
            mHarga = itemView.findViewById(R.id.textViewPrice);
            mImageDelete = itemView.findViewById(R.id.imageDelete);
            mButtonBeli = itemView.findViewById(R.id.buttonKlikBeli);

            mImageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (FJB_ListAdapter.this.mListener != null) {
                        int position = getAdapterPosition();
                        if (position != RecyclerView.NO_POSITION) {
                            FJB_ListAdapter.this.mListener.onItemClick(position);
                        }
                    }
                }
            });

            mButtonBeli.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (FJB_ListAdapter.this.mListener != null) {
                        int position = getAdapterPosition();
                        if (position != RecyclerView.NO_POSITION) {
                            FJB_ListAdapter.this.mListener.onViewWA(position);
                        }
                    }
                }
            });

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (FJB_ListAdapter.this.mListener != null) {
                        int position = getAdapterPosition();
                        if (position != RecyclerView.NO_POSITION) {
                            FJB_ListAdapter.this.mListener.onItemClick(position);
                        }
                    }
                }
            });
            mImageDelete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (FJB_ListAdapter.this.mListener != null) {
                        int position = getAdapterPosition();
                        if (position != RecyclerView.NO_POSITION) {
                            FJB_ListAdapter.this.mListener.onDeleteClick(position);
                        }
                    }
                }
            });
        }}


    public FJB_ListAdapter(ArrayList<FJB_List> fjb_lists){
        mFJBList = fjb_lists;
//Untuk FIlter :
        mFJBListFull = new ArrayList<>(fjb_lists);
    }

    @Override
    public FJBExampleViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.activity_fjb_list, parent, false);
        FJBExampleViewHolder fevh = new FJBExampleViewHolder(v, mListener);
        return fevh;
    }

    @Override
    public void onBindViewHolder(final FJBExampleViewHolder holder, int position) {
        final FJB_List currentItem = mFJBList.get(position);

        holder.mImageView.setImageResource(currentItem.getmImageResource());
        holder.mNamaBarang.setText(currentItem.getmNamaBarang());
        holder.mDeskripsi.setText(currentItem.getmDeskripsi());
        holder.mHarga.setText(currentItem.getmHarga());
    }

    @Override
    public int getItemCount() {
        return mFJBList.size();
    }

    //Untuk FIlter :
    @Override
    public Filter getFilter() {
        return exampleFilter;
    }

    private Filter exampleFilter = new Filter() {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            List<FJB_List> filteredList = new ArrayList<>();
            if (constraint == null || constraint.length() == 0) {
                filteredList.addAll(mFJBListFull);
            } else {
                String filterPattern = constraint.toString().toLowerCase().trim();
                for (FJB_List item : mFJBListFull) {
                    if (item.getmNamaBarang().toLowerCase().contains(filterPattern)) {
                        filteredList.add(item);
                    }
                }
            }
            FilterResults results = new FilterResults();
            results.values = filteredList;
            return results;
        }
        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            mFJBList.clear();
            mFJBList.addAll((List) results.values);
            notifyDataSetChanged();
        }
    };
        public void filterList(ArrayList<FJB_List> filteredList){
            mFJBList = filteredList;
            notifyDataSetChanged();
        }
}

