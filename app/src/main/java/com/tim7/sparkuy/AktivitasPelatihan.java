package com.tim7.sparkuy;

import android.content.Context;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.tim7.sparkuy.Model.Aktivitas;
import com.tim7.sparkuy.Model.AktivitasModel;

import java.util.ArrayList;


/**
 * A simple {@link Fragment} subclass.
 */
public class AktivitasPelatihan extends Fragment {
    Context context;
    DatabaseReference ref;
    FirebaseRecyclerOptions<AktivitasModel> options;
    FirebaseRecyclerAdapter<AktivitasModel, AktivitasRecyclerAdapter.AktivitasViewHolder> adapter;
    RecyclerView recyclerView;
    ArrayList<String> namaPemain;
    ArrayList<String> judullAktivitas;
    ArrayList<String> detailAktivitas;
    ArrayList<Aktivitas> list;
    AktivitasRecyclerAdapter aktivitasRecyclerAdapter;
    public AktivitasPelatihan() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_aktivitas_pelatihan, container, false);
        recyclerView = view.findViewById(R.id.aktivitasRecycler);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.addItemDecoration(new DividerItemDecoration(getActivity(), LinearLayoutManager.VERTICAL));

        ref = FirebaseDatabase.getInstance().getReference().child("Aktivitas");
        ref.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                list = new ArrayList<>();
                for (DataSnapshot ds : dataSnapshot.getChildren()){
                    list.add(ds.getValue(Aktivitas.class));
                }
                aktivitasRecyclerAdapter = new AktivitasRecyclerAdapter(context, list);
                recyclerView.setAdapter(aktivitasRecyclerAdapter);

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Toast.makeText(getContext(), "Upsie ada yang maslah nih", Toast.LENGTH_SHORT).show();
            }
        });
        return view ;
    }
}