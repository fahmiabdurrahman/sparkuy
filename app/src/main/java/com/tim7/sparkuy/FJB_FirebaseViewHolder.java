package com.tim7.sparkuy;

import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

public class FJB_FirebaseViewHolder extends RecyclerView.ViewHolder {
    private TextView deskripsi_barang, harga_barang, nama_barang;

    public FJB_FirebaseViewHolder(@NonNull View itemView) {
        super(itemView);

        nama_barang = itemView.findViewById(R.id.nama_barang);
        deskripsi_barang = itemView.findViewById(R.id.deskripsi_barang);
        harga_barang = itemView.findViewById(R.id.harga_barang);


    }
}


