package com.tim7.sparkuy;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

public class BerandaAktivitasAdapter extends RecyclerView.Adapter<BerandaAktivitasAdapter.myViewHolder> {
    Context context;
    ArrayList<String> judulAktivitasList;
    ArrayList<String> detailAktitivasList;

    @NonNull
    @Override
    public BerandaAktivitasAdapter.myViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_beranda_aktivitas_items, parent, false);
        return new myViewHolder(view);
    }

    public BerandaAktivitasAdapter() {
    }

    public BerandaAktivitasAdapter(Context context, ArrayList<String> judulAktivitasList, ArrayList<String> detailAktitivasList) {
        this.context = context;
        this.judulAktivitasList = judulAktivitasList;
        this.detailAktitivasList = detailAktitivasList;
    }

    @Override
    public void onBindViewHolder(@NonNull BerandaAktivitasAdapter.myViewHolder holder, int position) {
        holder.judul.setText(judulAktivitasList.get(position));
        holder.detail.setText(detailAktitivasList.get(position));
    }

    @Override
    public int getItemCount() {
        return judulAktivitasList.size();
    }

    public class myViewHolder extends RecyclerView.ViewHolder {
        TextView judul, detail;

        public myViewHolder(@NonNull View itemView) {
            super(itemView);
            judul = itemView.findViewById(R.id.judulAktivitas);
            detail = itemView.findViewById(R.id.detailAktivitas);
        }
    }
}
