package com.tim7.sparkuy;

public class FJB_UploadBarang {
    private String mName;
    private String mImageUrl;

    public FJB_UploadBarang(){

    }

    public FJB_UploadBarang(String name, String imageUrl){
        if (name.trim().equals("")) {
            name = "No Name";
        }
        mName = name;
        mImageUrl = imageUrl;
    }

    public String getName() {
        return mName;
    }

    public void setName(String mName) {
        this.mName = mName;
    }

    public String getImageUrl() {
        return mImageUrl;
    }

    public void setImageUrl(String mImageUrl) {
        this.mImageUrl = mImageUrl;
    }
}
