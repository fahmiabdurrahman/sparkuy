package com.tim7.sparkuy;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;

import org.w3c.dom.Text;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.MyViewHolder>  {

    ArrayList<PilihLapanganItems> list;
    Context mContext;
    DatabaseReference ref;
    public static final String NAMA_LAPANGAN = "com.tim7.sparkuy.nama_lapangan";
    public static final String ALAMAT_LAPANGAN = "com.tim7.sparkuy.alamat_lapangan";


    public RecyclerViewAdapter(ArrayList<PilihLapanganItems> list, Context mContext) {
        this.list = list;
        this.mContext = mContext;
    }

    @NonNull
    @Override
    public RecyclerViewAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.card_holder_items,parent,false);

        return new RecyclerViewAdapter.MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder holder, int position) {
        holder.namaLapangan.setText(list.get(position).getNama());
        Glide.with(mContext)
                .load(list.get(position).getFoto())
                .centerCrop()
                .placeholder(R.drawable.img_futsal_court)
                .into(holder.imageLapagnan);
        holder.alamatLapangan.setText(list.get(position).getAlamat());
        holder.detailLapangan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext, detail_lapangan.class);
                String nama_lapangan = holder.namaLapangan.getText().toString();
                String alamat_lapangan = holder.alamatLapangan.getText().toString();
                intent.putExtra(NAMA_LAPANGAN, nama_lapangan);
                intent.putExtra(ALAMAT_LAPANGAN, alamat_lapangan);
                mContext.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    static class MyViewHolder extends RecyclerView.ViewHolder {
        TextView namaLapangan, alamatLapangan, detailLapangan;
        ImageView imageLapagnan;
        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            namaLapangan = itemView.findViewById(R.id.cardLapanganName);
            imageLapagnan = itemView.findViewById(R.id.cardImage);
            alamatLapangan = itemView.findViewById(R.id.cardLapanganNameAddress);
            detailLapangan = itemView.findViewById(R.id.cardDetailLapangan);
        }
    }

    public void filter(String charText) {
        charText = charText.toLowerCase(Locale.getDefault());
        list.clear();
        if (charText.length() == 0) {
            ref.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    if (dataSnapshot.exists()) {
                        list = new ArrayList<>();
                        for (DataSnapshot ds : dataSnapshot.getChildren()) {
                            list.add(ds.getValue(PilihLapanganItems.class));
                        }
                    }
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {
                    Toast.makeText(mContext, databaseError.getMessage(), Toast.LENGTH_SHORT).show();
                }
            });
        } else {

            for (PilihLapanganItems object : list) {
                if (object.getNama().toLowerCase().contains(charText.toLowerCase())) {
                    list.add(object);
                }
            }
        }
    }
}
