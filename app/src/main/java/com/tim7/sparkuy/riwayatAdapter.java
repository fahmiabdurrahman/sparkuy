package com.tim7.sparkuy;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

public class riwayatAdapter extends RecyclerView.Adapter<riwayatAdapter.RiwayatViewHolder> {

    Context context;
    ArrayList<String> list;

    public riwayatAdapter(Context context, ArrayList<String> list) {
        this.context = context;
        this.list = list;
    }

    public riwayatAdapter() {
    }

    @NonNull
    @Override
    public riwayatAdapter.RiwayatViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_riwayat, parent, false);
        return new RiwayatViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull riwayatAdapter.RiwayatViewHolder holder, int position) {
        holder.orderId.setText(list.get(position));
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class RiwayatViewHolder extends RecyclerView.ViewHolder {
        TextView orderId;
        public RiwayatViewHolder(@NonNull View itemView) {
            super(itemView);
            orderId = itemView.findViewById(R.id.rp_table_generate_id1);
        }
    }
}
