package com.tim7.sparkuy;

import com.google.firebase.database.Exclude;

public class FJB_Teacher {
    private String mImageResources;
    private String mNamaBarang;
    private String mDeskripsi;
    private String mHarga;
    //gak tau ini apa
    private int position;
    private  String key;

    public FJB_Teacher(String trim, String s) {
        //empty constructor needed
    }
    public FJB_Teacher (int position){
        this.position = position;
    }
    public FJB_Teacher(String NamaBarang, String Deskripsi,  String ImageResource ,String Des, String harga) {
        if (NamaBarang.trim().equals("")) {
            NamaBarang = "No Name";
        }
        this.mImageResources = ImageResource;
        this.mNamaBarang = NamaBarang;
        this.mDeskripsi = Deskripsi;
        this.mHarga = harga;
    }

    public String getmImageResource() {
        return mImageResources;
    }

    public void setmImageResource(String mImageResource) {
        this.mImageResources = mImageResource;
    }

    public String getmNamaBarang() {
        return mNamaBarang;
    }

    public void setmNamaBarang(String mNamaBarang) {
        this.mNamaBarang = mNamaBarang;
    }

    public String getmDeskripsi() {
        return mDeskripsi;
    }

    public void setmDeskripsi(String mDeskripsi) {
        this.mDeskripsi = mDeskripsi;
    }

    public String getmHarga() {
        return mHarga;
    }

    public void setmHarga(String mHarga) {
        this.mHarga = mHarga;
    }
    @Exclude
    public String getKey() {
        return key;
    }
    @Exclude
    public void setKey(String key) {
        this.key = key;
    }
}
