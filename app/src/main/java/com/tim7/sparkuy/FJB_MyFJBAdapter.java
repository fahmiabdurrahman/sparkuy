package com.tim7.sparkuy;

import android.content.Context;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.squareup.picasso.Picasso;
import com.tim7.sparkuy.Model.MyFJB;

import java.util.ArrayList;
import java.util.List;

public class FJB_MyFJBAdapter extends FirebaseRecyclerAdapter<FJB_ForMyList, FJB_MyFJBAdapter.PastViewHolder> {

    public FJB_MyFJBAdapter(@NonNull FirebaseRecyclerOptions<FJB_ForMyList> options) {
        super(options);
    }

    @Override
        protected void onBindViewHolder(@NonNull PastViewHolder holder, int i, @NonNull FJB_ForMyList post) {
            holder.nama_barang.setText(post.getNama_barang());
            holder.deskripsi_barang.setText(post.getDeskripsi_barang());
            holder.harga_barang.setText(post.getHarga_barang());
        }

        @NonNull
        @Override
        public PastViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.activity_fjb_row, parent, false);
            return new PastViewHolder(view);
        }

    class PastViewHolder extends RecyclerView.ViewHolder{
            TextView nama_barang, deskripsi_barang, harga_barang;
            public PastViewHolder(@NonNull View itemView) {
                super(itemView);
                nama_barang = itemView.findViewById(R.id.nama_barang);
                deskripsi_barang = itemView.findViewById(R.id.deskripsi_barang);
                harga_barang = itemView.findViewById(R.id.harga_barang);
            }
        }
    }