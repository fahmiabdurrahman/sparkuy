package com.tim7.sparkuy;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import static com.tim7.sparkuy.tambahteman.KEY_TEMAN;

public class HapusTeman extends AppCompatActivity {
    DatabaseReference ref;
    String iniKey;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hapus_teman);
        Intent intent = getIntent();
        final String key_teeman = intent.getStringExtra(KEY_TEMAN);
        iniKey = key_teeman;
        DisplayMetrics dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);

        int width = dm.widthPixels;
        int height = dm.heightPixels;

        getWindow().setLayout((int)(width*.7), (int)(height*.5));

        WindowManager.LayoutParams params = getWindow().getAttributes();
        params.gravity = Gravity.CENTER;
        params.x = 0;
        params.y = -20;

        getWindow().setAttributes(params);
    }

    public void yesButton(View view) {
        ref = FirebaseDatabase.getInstance().getReference().child("Sosial").child(FirebaseAuth.getInstance().getCurrentUser().getUid());
        ref.child(iniKey).removeValue();
        Toast.makeText(this, "Berhasil", Toast.LENGTH_SHORT).show();
        onBackPressed();
    }

    public void batalButton(View view) {
        onBackPressed();
    }
}
