package com.tim7.sparkuy;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Locale;

import static com.tim7.sparkuy.RecyclerViewAdapter.ALAMAT_LAPANGAN;
import static com.tim7.sparkuy.RecyclerViewAdapter.NAMA_LAPANGAN;

public class pilih_lapangan_activity extends AppCompatActivity {

    DatabaseReference ref;
    ArrayList<PilihLapanganItems> list;
    RecyclerView recyclerView;
    EditText searchbar;
    ArrayList<String> namaLapangan;
    ArrayList<String> alamatLapangan;
    RecyclerViewAdapter recyclerViewAdapter;
    FirebaseRecyclerOptions<PilihLapanganItems> options;
    FirebaseRecyclerAdapter<PilihLapanganItems, RecyclerViewAdapter.MyViewHolder> adapter;
    Context mContext;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pilih_lapangan_activity);

        ref = FirebaseDatabase.getInstance().getReference().child("lapangan");
        recyclerView = findViewById(R.id.recyclerView);
        searchbar = findViewById(R.id.search_bar);

        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.addItemDecoration(new DividerItemDecoration(this, LinearLayoutManager.VERTICAL));

        namaLapangan = new ArrayList<>();
        alamatLapangan = new ArrayList<>();

        searchbar.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (!s.toString().isEmpty()) {
                    setAdapter(s.toString());
                } else {
                    onStart();
                }
            }
        });
        BottomNavigationView bottomNavigationView = findViewById(R.id.bottom_navigation);

        bottomNavigationView.setSelectedItemId(R.id.nav_sparing);

        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                switch (menuItem.getItemId()){
                    case R.id.nav_beranda:
                        startActivity(new Intent(getApplicationContext(), MainActivity.class));
                        return true;
                    case R.id.nav_sparing:
                        startActivity(new Intent(getApplicationContext(), pilih_lapangan_activity.class));
                        return true;
                    case R.id.nav_akun:
                        startActivity(new Intent(getApplicationContext(), akun_profile.class));
                        return true;
                }
                return false;
            }
        });

    }

    private void setAdapter(String toString) {
        String searchText = toString.toUpperCase();
        Query query = ref.orderByChild("nama").startAt(searchText).endAt(searchText+"\uf8ff");
        options = new FirebaseRecyclerOptions.Builder<PilihLapanganItems>().setQuery(query, PilihLapanganItems.class).build();
        adapter = new FirebaseRecyclerAdapter<PilihLapanganItems, RecyclerViewAdapter.MyViewHolder>(options) {
            @Override
            protected void onBindViewHolder(@NonNull final RecyclerViewAdapter.MyViewHolder holder, int position, @NonNull PilihLapanganItems model) {
                holder.namaLapangan.setText(model.getNama());
                holder.imageLapagnan.setImageResource(R.drawable.img_futsal_court);
                holder.alamatLapangan.setText(model.getAlamat());
                holder.detailLapangan.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(pilih_lapangan_activity.this, detail_lapangan.class);
                        String nama_lapangan = holder.namaLapangan.getText().toString();
                        String alamat_lapangan = holder.alamatLapangan.getText().toString();
                        intent.putExtra(NAMA_LAPANGAN, nama_lapangan);
                        intent.putExtra(ALAMAT_LAPANGAN, alamat_lapangan);
                        pilih_lapangan_activity.this.startActivity(intent);
                    }
                });
            }

            @NonNull
            @Override
            public RecyclerViewAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
                View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_holder_items, parent, false);

                return new RecyclerViewAdapter.MyViewHolder(v);
            }
        };
        adapter.startListening();
        recyclerView.setAdapter(adapter);
    }

    @Override
    protected void onStart() {
        super.onStart();
        searchbar.clearFocus();
        if (ref != null) {
            ref.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    if (dataSnapshot.exists()) {
                        list = new ArrayList<>();
                        for (DataSnapshot ds : dataSnapshot.getChildren()) {
                            list.add(ds.getValue(PilihLapanganItems.class));
                        }
                        System.out.println(list);
                        RecyclerViewAdapter recyclerViewAdapter = new RecyclerViewAdapter(list, pilih_lapangan_activity.this);
                        recyclerView.setAdapter(recyclerViewAdapter);
                    }
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {
                    Toast.makeText(pilih_lapangan_activity.this, databaseError.getMessage(), Toast.LENGTH_SHORT).show();
                }
            });
        }
    }
}

