package com.tim7.sparkuy;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

import static com.tim7.sparkuy.RecyclerViewAdapter.NAMA_LAPANGAN;

public class DetailPertandinganSolo extends AppCompatActivity implements AdapterView.OnItemSelectedListener {
    DatabaseReference ref, counterRef;
    Button booking;
    TextView namaTimA, gkA, anchorA, pivotA, flank1A, flank2A;
    TextView namaTimB, gkB, anchorB, pivotB, flank1B, flank2B;
    Spinner spinnerTeam, spinnerPosisi;
    ArrayList<String> timArray = new ArrayList<>();
    ArrayList<String> posisiArray = new ArrayList<>();
    int counterPemain = 0;
    String AnchorAString, AnchorBString, GkAString, GkBString, PivotAString, PivotBString, Flank1AString, Flank2AString, Flank1BString, Flank2BString;
    String timPilihan, posisiPilihan;
    String biaya = "30000";
    public static final String BIAYA = "com.tim7.sparkuy.biaya";
    public static final String JADWAL_PERTANDINGAN = "com.tim7.sparkuy.jadwal_pertandingan";
    public static final String POSISI = "com.tim7.sparkuy.posisi";
    public static final String TIM_PILIHAN = "com.tim7.sparkuy.tim_pilihan";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_pertandingan_solo);
        Intent intent = getIntent();
        final String nama_lapangan = intent.getStringExtra(NAMA_LAPANGAN);
        final String jadwal_pertandingan_solo = intent.getStringExtra(JadwalPertandingan.JADWAL_PERTANDINGAN);
        ref = FirebaseDatabase.getInstance().getReference("detail_pertandingan_lapangan").child(nama_lapangan).child("waiting_match");
        counterRef = FirebaseDatabase.getInstance().getReference("detail_pertandingan_lapangan").child(nama_lapangan).child("pemain_menunggu");
        namaTimA = findViewById(R.id.timALabelDetailPertandingan);
        gkA = findViewById(R.id.GKPlayerA);
        anchorA = findViewById(R.id.AnchorPlayerA);
        pivotA = findViewById(R.id.PivotPlayerA);
        flank1A = findViewById(R.id.FlankPlayer1A);
        flank2A = findViewById(R.id.FlankPlayer2A);
        //--
        namaTimB = findViewById(R.id.timBLabelDetailPertandingan);
        gkB = findViewById(R.id.GKPlayerB);
        anchorB = findViewById(R.id.AnchorPlayerB);
        pivotB = findViewById(R.id.PivotPlayerB);
        flank1B = findViewById(R.id.FlankPlayer1B);
        flank2B = findViewById(R.id.FlankPlayer2B);

        spinnerTeam = findViewById(R.id.spinnerTeam);
        spinnerPosisi = findViewById(R.id.spinnerPosisi);
        readData(new FirebaseCallback() {
            @Override
            public void AnchorA(String anchorAString) {
                AnchorAString = anchorAString;
            }

            @Override
            public void AnchorB(String anchorBString) {
                AnchorBString = anchorBString;
            }

            @Override
            public void GKA(String gkAString) {
                GkAString = gkAString;
            }

            @Override
            public void GKB(String gkBString) {
                GkBString = gkBString;
            }

            @Override
            public void Flank1A(String flank1AString) {
                Flank1AString = flank1AString;
            }

            @Override
            public void Flank2A(String flank2AString) {
                Flank2AString = flank2AString;
            }

            @Override
            public void Flank1B(String flank1BString) {
                Flank1BString = flank1BString;
            }

            @Override
            public void Flank2B(String flank2BString) {
                Flank2BString = flank2BString;
            }

            @Override
            public void PivotA(String pivotAString) {
                PivotAString = pivotAString;
            }

            @Override
            public void PivotB(String pivotBString) {
                PivotBString = pivotBString;
            }
        });
        // menambahkan TIM ke arraylist
        timArray.add(0, "--Pilih Tim--");
        timArray.add("TEAM A");
        timArray.add("TEAM B");

        // menambahkan posisi ke arraylist
        posisiArray.add(0, "--Pilih Posisi--");


        populateSpinner();

        booking = findViewById(R.id.booking);
        booking.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                counterPemain--;
                if (counterPemain < 0){
                    counterPemain = 0;
                }

                counterRef.setValue(counterPemain);
                Intent intent = new Intent(DetailPertandinganSolo.this, DetailBooking.class);
                intent.putExtra(NAMA_LAPANGAN, nama_lapangan);
                intent.putExtra(JADWAL_PERTANDINGAN,jadwal_pertandingan_solo);
                intent.putExtra(BIAYA, biaya);
                intent.putExtra(POSISI, posisiPilihan);
                intent.putExtra(TIM_PILIHAN, timPilihan);
                startActivity(intent);
            }
        });


        // membuat tampilan seperti pop up
        DisplayMetrics dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);

        int width = dm.widthPixels;
        int height = dm.heightPixels;

        getWindow().setLayout((int)(width), (int)(height*.7));

        WindowManager.LayoutParams params = getWindow().getAttributes();
        params.gravity = Gravity.BOTTOM;
        params.x = 0;
        params.y = 0;

        getWindow().setAttributes(params);
    }


    private void readData(final FirebaseCallback firebaseCallback) {
        DatabaseReference timAref = ref.child("TEAM A");
        DatabaseReference anchorAref = timAref.child("ANCHOR");
        anchorAref.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                anchorA.setText(dataSnapshot.getValue().toString());
                firebaseCallback.AnchorA(anchorA.getText().toString());
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
        DatabaseReference flank1Aref = timAref.child("FLANK 1");
        flank1Aref.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                flank1A.setText(dataSnapshot.getValue().toString());
                firebaseCallback.Flank1A(flank1A.getText().toString());
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
        DatabaseReference flank2Aref = timAref.child("FLANK 2");
        flank2Aref.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                flank2A.setText(dataSnapshot.getValue().toString());
                firebaseCallback.Flank2A(flank2A.getText().toString());
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
        DatabaseReference gkAref = timAref.child("GK");
        gkAref.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                gkA.setText(dataSnapshot.getValue().toString());
                firebaseCallback.GKA(gkA.getText().toString());
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
        DatabaseReference pivotAref = timAref.child("PIVOT");
        pivotAref.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                pivotA.setText(dataSnapshot.getValue().toString());
                firebaseCallback.PivotA(pivotA.getText().toString());
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
        DatabaseReference timBref = ref.child("TEAM B");
        DatabaseReference anchorBref = timBref.child("ANCHOR");
        anchorBref.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                anchorB.setText(dataSnapshot.getValue().toString());
                firebaseCallback.AnchorB(anchorB.getText().toString());
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
        DatabaseReference flank1BRef = timBref.child("FLANK 1");
        flank1BRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                flank1B.setText(dataSnapshot.getValue().toString());
                firebaseCallback.Flank1B(flank1B.getText().toString());
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
        DatabaseReference flank2BRef = timBref.child("FLANK 2");
        flank2BRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                flank2B.setText(dataSnapshot.getValue().toString());
                firebaseCallback.Flank2B(flank2B.getText().toString());
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
        DatabaseReference gkBref = timBref.child("GK");
        gkBref.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                gkB.setText(dataSnapshot.getValue().toString());
                firebaseCallback.GKB(gkB.getText().toString());
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
        DatabaseReference pivotBref = timBref.child("PIVOT");
        pivotBref.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                pivotB.setText(dataSnapshot.getValue().toString());
                firebaseCallback.PivotB(pivotB.getText().toString());
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
        counterRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                counterPemain = Integer.parseInt(dataSnapshot.getValue().toString());
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    private interface FirebaseCallback {
        void AnchorA(String anchorAString);

        void AnchorB(String anchorBString);

        void GKA(String gkAString);

        void GKB(String gkBString);

        void Flank1A(String flank1AString);

        void Flank2A(String flank2AString);

        void Flank1B(String flank1BString);

        void Flank2B(String flank2BString);

        void PivotA(String pivotAString);

        void PivotB(String pivotBString);

    }


    public void populateSpinner() {
        // permainan pengondisian dimulai

        if (spinnerTeam != null) {
            spinnerTeam.setOnItemSelectedListener(this);
        }
        if (spinnerPosisi != null) {
            spinnerPosisi.setOnItemSelectedListener(this);
        }
        ArrayAdapter<String> adapterTeam = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, timArray);
        ArrayAdapter<String> adapterPosisi = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, posisiArray);
        adapterTeam.setDropDownViewResource
                (android.R.layout.simple_spinner_dropdown_item);
        adapterPosisi.setDropDownViewResource
                (android.R.layout.simple_spinner_dropdown_item);
        if (spinnerTeam != null) {
            spinnerTeam.setAdapter(adapterTeam);
        }
        if (spinnerPosisi != null) {
            spinnerPosisi.setAdapter(adapterPosisi);
        }
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        if (parent.getId() == R.id.spinnerTeam) {
            String selectedTeam = parent.getItemAtPosition(position).toString();
            if (parent.getItemAtPosition(position).equals("--Pilih Tim--")) {
                if (posisiArray.size() > 1) {
                    posisiArray.clear();
                    posisiArray.add(0, "--Pilih Posisi--");
                }
            } else if (parent.getItemAtPosition(position).equals("TEAM A")) {
                posisiArray.clear();
                posisiArray.add(0, "--Pilih Posisi--");
                posisiArray.add("ANCHOR");
                posisiArray.add("FLANK 1");
                posisiArray.add("FLANK 2");
                posisiArray.add("GK");
                posisiArray.add("PIVOT");
                if (!AnchorAString.equals("tersedia")) {
                    posisiArray.remove("ANCHOR");
                }
                if (!Flank1AString.equals("tersedia")) {
                    posisiArray.remove("FLANK 1");
                }
                if (!Flank2AString.equals("tersedia")) {
                    posisiArray.remove("FLANK 2");
                }
                if (!GkAString.equals("tersedia")) {
                    posisiArray.remove("GK");
                }
                if (!PivotAString.equals("tersedia")) {
                    posisiArray.remove("PIVOT");
                }             Toast.makeText(this, selectedTeam, Toast.LENGTH_SHORT).show();

            } else if (parent.getItemAtPosition(position).equals("TEAM B")) {
                posisiArray.clear();
                posisiArray.add(0, "--Pilih Posisi--");
                posisiArray.add("ANCHOR");
                posisiArray.add("FLANK 1");
                posisiArray.add("FLANK 2");
                posisiArray.add("GK");
                posisiArray.add("PIVOT");
                if (!AnchorBString.equals("tersedia")) {
                    posisiArray.remove("ANCHOR");
                }
                if (!Flank1BString.equals("tersedia")) {
                    posisiArray.remove("FLANK 1");
                }
                if (!Flank2BString.equals("tersedia")) {
                    posisiArray.remove("FLANK 2");
                }
                if (!GkBString.equals("tersedia")) {
                    posisiArray.remove("GK");
                }
                if (!PivotBString.equals("tersedia")) {
                    posisiArray.remove("PIVOT");
                }
                Toast.makeText(this, selectedTeam, Toast.LENGTH_SHORT).show();

            }
            timPilihan = selectedTeam;
        } else if (parent.getId() == R.id.spinnerPosisi) {
            if (parent.getItemAtPosition(position).equals("--Pilih Posisi--")) {
                // tidak melakukan apapun
            } else {

                String selectedPosisi = parent.getItemAtPosition(position).toString();
                Toast.makeText(this, selectedPosisi, Toast.LENGTH_SHORT).show();
                posisiPilihan = selectedPosisi;
            }

        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {

    }
}
