package com.tim7.sparkuy;


import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.google.firebase.database.DatabaseReference;
import com.tim7.sparkuy.Model.User;

import java.util.ArrayList;

import static com.tim7.sparkuy.DetailBooking.NAMA_PEMAIN;

public class TemanAdapter extends RecyclerView.Adapter<TemanAdapter.MyViewHolder>{

    Context context;
    ArrayList<String> list;
    DatabaseReference ref;
    public TemanAdapter(Context c, ArrayList<String> p){
        context = c;
        list = p;
    }

    public TemanAdapter() {
    }

    @NonNull
    @Override
    public TemanAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.cari_teman_holder,parent,false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder holder, int i) {
        holder.nama.setText(list.get(i));
        holder.namaTeman.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context,tambahteman.class);
                String namaTemanStr = holder.nama.getText().toString();
                intent.putExtra(NAMA_PEMAIN, namaTemanStr);
                context.startActivity(intent);
            }
        });

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public void filterlist(ArrayList<String> filteredList) {
        list = filteredList;
        notifyDataSetChanged();
    }

    static class MyViewHolder extends RecyclerView.ViewHolder{

        TextView nama;
        LinearLayout namaTeman;
        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            namaTeman = itemView.findViewById(R.id.namaTemanSearch);
            nama = itemView.findViewById(R.id.cariNamaTemen);
        }
    }
}
