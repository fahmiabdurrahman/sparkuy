package com.tim7.sparkuy;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.tim7.sparkuy.Model.Aktivitas;
import com.tim7.sparkuy.Model.AktivitasModel;

import java.util.ArrayList;

public class AktivitasRecyclerAdapter extends RecyclerView.Adapter<AktivitasRecyclerAdapter.AktivitasViewHolder> {

    Context context;
    ArrayList<Aktivitas> model;
    public AktivitasRecyclerAdapter(Context c , ArrayList<Aktivitas> p){
        context = c;
        model = p;
    }
    @NonNull
    @Override
    public AktivitasRecyclerAdapter.AktivitasViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_aktivitas, parent, false);
        return new AktivitasViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull AktivitasRecyclerAdapter.AktivitasViewHolder holder, int position) {
        holder.namaPemain.setText(model.get(position).getNamaPemain());
        holder.aktivitasPemain.setText(model.get(position).getAktivitas());
        holder.detailAktivitasPemain.setText(model.get(position).getDetailAktivitas());
    }

    @Override
    public int getItemCount() {
        return  model.size();
    }

    public static class AktivitasViewHolder extends RecyclerView.ViewHolder {
        TextView namaPemain, aktivitasPemain, detailAktivitasPemain;
        public AktivitasViewHolder(@NonNull View itemView) {
            super(itemView);
            namaPemain = itemView.findViewById(R.id.namaPemainAktivitas);
            aktivitasPemain = itemView.findViewById(R.id.aktivitasPemain);
            detailAktivitasPemain = itemView.findViewById(R.id.detailAktivitasPemain);
        }
    }
}