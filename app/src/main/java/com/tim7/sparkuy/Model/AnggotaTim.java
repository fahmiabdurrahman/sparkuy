package com.tim7.sparkuy.Model;

public class AnggotaTim {
    public String nama_anggota;

    public AnggotaTim() {
    }

    public String getNama_anggota() {
        return nama_anggota;
    }

    public void setNama_anggota(String nama_anggota) {
        this.nama_anggota = nama_anggota;
    }

    public AnggotaTim(String nama_anggota) {
        this.nama_anggota = nama_anggota;
    }
}
