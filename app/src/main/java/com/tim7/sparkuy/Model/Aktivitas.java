package com.tim7.sparkuy.Model;

public class Aktivitas {
    public Aktivitas() {
    }

    private String aktivitas, detailAktivitas, namaPemain;

    public String getNamaPemain() {
        return namaPemain;
    }

    public void setNamaPemain(String namaPemain) {
        this.namaPemain = namaPemain;
    }

    public Aktivitas(String aktivitas, String detailAktivitas, String namaPemain) {
        this.aktivitas = aktivitas;
        this.detailAktivitas = detailAktivitas;
        this.namaPemain = namaPemain;
    }

    public String getAktivitas() {
        return aktivitas;
    }

    public void setAktivitas(String aktivitas) {
        this.aktivitas = aktivitas;
    }

    public String getDetailAktivitas() {
        return detailAktivitas;
    }

    public void setDetailAktivitas(String detailAktivitas) {
        this.detailAktivitas = detailAktivitas;
    }
}
