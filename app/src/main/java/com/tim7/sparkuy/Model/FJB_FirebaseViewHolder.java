package com.tim7.sparkuy.Model;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.tim7.sparkuy.R;

public class FJB_FirebaseViewHolder extends RecyclerView.ViewHolder {
    private final Object ImageView;
    public TextView textViewTitle, textViewDesc, textViewPrice;
    public ImageView imageBarang;

    public FJB_FirebaseViewHolder(@NonNull View itemView) {
        super(itemView);

        textViewTitle = itemView.findViewById(R.id.textViewTitle);
        textViewDesc = itemView.findViewById(R.id.textViewDesc);
        textViewPrice = itemView.findViewById(R.id.textViewPrice);
        ImageView = itemView.findViewById(R.id.imageBarang);
    }
}
