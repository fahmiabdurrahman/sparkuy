package com.tim7.sparkuy.Model;

public class AjuanPertanyaan {
    private String subject;
    private String pertanyaan;

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getPertanyaan() {
        return pertanyaan;
    }

    public void setPertanyaan(String pertanyaan) {
        this.pertanyaan = pertanyaan;
    }

    public AjuanPertanyaan() {

    }
}
