package com.tim7.sparkuy.Model;

public class AktivitasModel {
    private String namaPemain;
    private String aktivitasPemain;

    public String getNamaPemain() {
        return namaPemain;
    }

    public void setNamaPemain(String namaPemain) {
        this.namaPemain = namaPemain;
    }


    public AktivitasModel() {
    }

    public String getAktivitasPemain() {
        return aktivitasPemain;
    }

    public void setAktivitasPemain(String aktivitasPemain) {
        this.aktivitasPemain = aktivitasPemain;
    }

    public AktivitasModel(String namaPemain, String aktivitasPemain) {
        this.namaPemain = namaPemain;
        this.aktivitasPemain = aktivitasPemain;
    }
}