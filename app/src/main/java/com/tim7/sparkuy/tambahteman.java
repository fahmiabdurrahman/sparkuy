package com.tim7.sparkuy;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;


import androidx.appcompat.app.AppCompatActivity;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import static com.tim7.sparkuy.DetailBooking.NAMA_PEMAIN;

public class tambahteman extends AppCompatActivity {
    Button changeButtonText, tambahTeman;
    private TextView namaTeman;
    DatabaseReference ref, userRef;
    String keyTeman;
    public static final String KEY_TEMAN = "com.tim7.sparkuy.key_teman";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tambahteman);
        ref = FirebaseDatabase.getInstance().getReference().child("Sosial").child(FirebaseAuth.getInstance().getCurrentUser().getUid());
        namaTeman = findViewById(R.id.labelnama);
        tambahTeman = findViewById(R.id.tambahkantemanButton);
        tambahTeman.setText("Tambahkan Teman");
        tambahTeman.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (tambahTeman.getText().equals("Tambahkan Teman")){
                    DatabaseReference hiya = ref.push();
                    hiya.setValue(namaTeman.getText().toString());
                    keyTeman = hiya.getKey();
                    Toast.makeText(tambahteman.this, "Tambah Teman Berhasil!", Toast.LENGTH_SHORT).show();
                    tambahTeman.setText("Hapus Teman");
                } else {
                    Intent intent = new Intent(tambahteman.this, HapusTeman.class);
                    intent.putExtra(KEY_TEMAN, keyTeman);
                    startActivity(intent);
                }
            }
        });
        Intent intent = getIntent();
        final String nama_teman = intent.getStringExtra(NAMA_PEMAIN);
        namaTeman.setText(nama_teman);


    }

    public void tambahteman(View view) {
        changeButtonText = (Button) findViewById(R.id.tambahkantemanButton);

               changeButtonText.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    changeButtonText.setText("Hapus Teman");

                        startActivity(new Intent(getApplicationContext(), HapusTeman.class));
                    }
            });

    }

    public void pesanButton(View view) {

        Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);
        sendIntent.putExtra(Intent.EXTRA_TEXT, "Hai "+namaTeman+"! Ayo bermain!");
        sendIntent.setType("text/plain");
        sendIntent.setPackage("com.whatsapp");
        startActivity(sendIntent);
    }
}
