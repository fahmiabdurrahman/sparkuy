package com.tim7.sparkuy;

public class PilihLapanganItems {
    private String foto;
    private String nama;
    private String alamat;

    public PilihLapanganItems(){
        //konstruktor kosong
    }

    public PilihLapanganItems(String imageLapangan, String namaLapangan, String alamatLapagnan){
        this.foto = imageLapangan;
        this.nama = namaLapangan;
        this.alamat = alamatLapagnan;
    }

    public String getFoto() {
        return foto;
    }

    public void setFoto(String foto) {
        this.foto = foto;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getAlamat() {
        return alamat;
    }

    public void setAlamat(String alamat) {
        this.alamat = alamat;
    }
}
