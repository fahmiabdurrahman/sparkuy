package com.tim7.sparkuy;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class buatTimActivity extends AppCompatActivity {
    DatabaseReference ref, userRef;
    String namaPemain;
    EditText timEditText;
    Button submitButton;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_buat_tim);
        timEditText = findViewById(R.id.namaTimEditText);
        submitButton = findViewById(R.id.submitButton);
        userRef = FirebaseDatabase.getInstance().getReference("User").child(FirebaseAuth.getInstance().getCurrentUser().getUid()).child("nama");
        userRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                namaPemain = dataSnapshot.getValue().toString();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
        final String getNama = timEditText.getText().toString();
        submitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DatabaseReference userTimRef = FirebaseDatabase.getInstance().getReference("User").child(FirebaseAuth.getInstance().getCurrentUser().getUid()).child("tim");
                userTimRef.setValue(getNama);
                ref = FirebaseDatabase.getInstance().getReference("tim").child(getNama);
                ref.child("counterAnggota").setValue(1);
                ref.child("Nama Tim").setValue(getNama);
                ref.child("1").setValue(namaPemain);
                Toast.makeText(buatTimActivity.this, "Berhasil menambahkan tim!", Toast.LENGTH_SHORT).show();
                finish();
            }
        });

    }
}
