package com.tim7.sparkuy;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import androidx.appcompat.app.AppCompatActivity;

public class pusat_bantuan extends AppCompatActivity {

    private static final String TAG = "pusat_bantuan";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.pusat_bantuan);
        Log.d(TAG, "onCreate: Starting.");

        Button pb_tambah_teman = (Button) findViewById(R.id.pb_tambah_teman);
        Button pb_cara_pembayaran = (Button) findViewById(R.id.pb_cara_pembayaran);
        Button pb_cara_belanja = (Button) findViewById(R.id.pb_cara_belanja);
        Button pb_header_masukan = (Button) findViewById(R.id.pb_header_masukan);
        Button pb_pertanyaan = (Button) findViewById(R.id.pb_pertanyaan);

        pb_tambah_teman.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(TAG, "onClick: pb_tambah_teman.");

                Intent a = new Intent(pusat_bantuan.this, pb_tambah_teman.class);
                startActivity(a);
            }
        });

        pb_cara_pembayaran.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Log.d(TAG, "onClick: pb_tambah_teman.");

                    Intent intent = new Intent(pusat_bantuan.this, pb_cara_pembayaran.class);
                    startActivity(intent);
                }
        });

        pb_cara_belanja.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Log.d(TAG, "onClick: pb_tambah_teman.");

                            Intent intent = new Intent(pusat_bantuan.this, pb_belanja.class);
                            startActivity(intent);
                        }
        });

        pb_header_masukan.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Log.d(TAG, "onClick: pb_tambah_teman.");

                    Intent intent = new Intent(pusat_bantuan.this, pb_masukan.class);
                    startActivity(intent);
                }
        });

        pb_pertanyaan.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Log.d(TAG, "onClick: pb_pertanyaann.");

                            Intent intent = new Intent  (pusat_bantuan.this, pb_pertanyaan.class);
                            startActivity(intent);
                        }

        });



    }}
