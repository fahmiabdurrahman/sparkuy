package com.tim7.sparkuy;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

public class Login extends AppCompatActivity {

    EditText mEmail, mPassword;
    Button masukButton;
    ProgressBar progressBar;
    FirebaseAuth fAuth;
    TextView backAgain;
    ImageView backButton;
    CheckBox loginCheckbox;
    private SharedPreferences mPreferences;
    private SharedPreferences.Editor mEditor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        loginCheckbox = findViewById(R.id.checkboxLogin);
        mEmail = findViewById(R.id.email_text);
        mPassword = findViewById(R.id.password_text);
        progressBar = findViewById(R.id.progressBar);
        fAuth = FirebaseAuth.getInstance();
        masukButton = findViewById(R.id.msk_btn);

        backButton = findViewById(R.id.white_back_buttoon);
        backAgain = findViewById(R.id.masukAfterBack);
        mPreferences = PreferenceManager.getDefaultSharedPreferences(this);
//        mPreferences = getSharedPreferences("com.tim7.sparkuy", Context.MODE_PRIVATE);
        mEditor = mPreferences.edit();
        checkSharedPreferences();
        backAgain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        masukButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String email = mEmail.getText().toString().trim();
                String password = mPassword.getText().toString().trim();
                if (loginCheckbox.isChecked()){
                    // pilih chcekbox
                    mEditor.putString(getString(R.string.checkbox), "True");
                    mEditor.commit();

                    // simpan email
                    mEditor.putString(getString(R.string.email), email);
                    mEditor.commit();

                    // simpan password
                    mEditor.putString(getString(R.string.password), password);
                    mEditor.commit();
                } else {
                    // pilih chcekbox
                    mEditor.putString(getString(R.string.checkbox), "False");
                    mEditor.commit();

                    // simpan email
                    mEditor.putString(getString(R.string.email), "");
                    mEditor.commit();

                    // simpan password
                    mEditor.putString(getString(R.string.password), "");
                    mEditor.commit();
                }

                if (TextUtils.isEmpty(email)) {
                    mEmail.setError("Masukkan email kamu");
                    return;
                }
                if (TextUtils.isEmpty(password)) {
                    mPassword.setError("Masukkan Password");
                    return;
                }
                if (password.length() < 5) {
                    mPassword.setError("Password min. 5 karakter");
                    return;
                }

                progressBar.setVisibility(View.VISIBLE);

                //authenticate the user

                fAuth.signInWithEmailAndPassword(email, password).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            progressBar.setVisibility(View.GONE);
                            finish();
                            Toast.makeText(Login.this, "Berhasil masuk", Toast.LENGTH_SHORT).show();
                            startActivity(new Intent(getApplicationContext(), MainActivity.class));
                        } else {
                            progressBar.setVisibility(View.GONE);
                            Toast.makeText(Login.this, "Error " + task.getException().getMessage(), Toast.LENGTH_SHORT).show();
                            progressBar.setVisibility(View.GONE);
                        }
                    }
                });
            }


        });
    }

    private void checkSharedPreferences(){
        String checkbox = mPreferences.getString(getString(R.string.checkbox), "False");
        String email = mPreferences.getString(getString(R.string.email), "");
        String password = mPreferences.getString(getString(R.string.password), "");

        mEmail.setText(email);
        mPassword.setText(password);

        if (checkbox.equals("True")){
            loginCheckbox.setChecked(true);
        } else {
            loginCheckbox.setChecked(false);
        }
    }
}


