package com.tim7.sparkuy;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

import static com.tim7.sparkuy.DetailPertandinganSolo.BIAYA;
import static com.tim7.sparkuy.DetailPertandinganSolo.POSISI;
import static com.tim7.sparkuy.DetailPertandinganSolo.TIM_PILIHAN;
import static com.tim7.sparkuy.JadwalPertandingan.JADWAL_PERTANDINGAN;
import static com.tim7.sparkuy.RecyclerViewAdapter.NAMA_LAPANGAN;

public class DetailPertandinganTim extends AppCompatActivity implements AdapterView.OnItemSelectedListener{
    Button booking;
    RelativeLayout relativeLayout;
    String biaya = "150000";
    int counterPemain = 0;
    String jadwalBertanding;
    Spinner spinnerTanggal;
    ArrayList<String> jadwalArray = new ArrayList<>();
    DatabaseReference ref, counterRef;
    TextView namatim;
    String namaTim;
    String posisi = "NULL";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_pertandingan_tim);
        Intent intent = getIntent();
        final String nama_lapangan = intent.getStringExtra(NAMA_LAPANGAN);
        counterRef = FirebaseDatabase.getInstance().getReference("detail_pertandingan_lapangan").child(nama_lapangan).child("pemain_menunggu");
        relativeLayout = findViewById(R.id.relativeLayoutDetailPerandinganInside);
        relativeLayout.setVisibility(View.GONE);
        namatim = findViewById(R.id.timALabelDetailPertandingan);
        namaTim = namatim.getText().toString();
        spinnerTanggal = findViewById(R.id.spinnerTanggal);
        readData();
        if (spinnerTanggal != null) {
            spinnerTanggal.setOnItemSelectedListener(this); }
        ArrayAdapter<CharSequence> adapterTanggal = ArrayAdapter.createFromResource(this,
                R.array.spinner_tanggal, android.R.layout.simple_spinner_item);
        adapterTanggal.setDropDownViewResource
                (android.R.layout.simple_spinner_dropdown_item);
        if (spinnerTanggal != null) {
            spinnerTanggal.setAdapter(adapterTanggal);
        }
        booking = findViewById(R.id.booking);
        booking.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                counterPemain--;
                if (counterPemain < 0){
                    counterPemain = 0;
                }
                counterRef.setValue(counterPemain);
                Intent intent = new Intent(DetailPertandinganTim.this, DetailBooking.class);
                intent.putExtra(NAMA_LAPANGAN, nama_lapangan);
                intent.putExtra(JADWAL_PERTANDINGAN, jadwalBertanding);
                intent.putExtra(BIAYA, biaya);
                intent.putExtra(POSISI, posisi);
                intent.putExtra(TIM_PILIHAN, namaTim);
                startActivity(intent);
            }
        });
        // membuat tampilan seperti pop up
        DisplayMetrics dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);

        int width = dm.widthPixels;
        int height = dm.heightPixels;

        getWindow().setLayout((int)(width), (int)(height*.6));

        WindowManager.LayoutParams params = getWindow().getAttributes();
        params.gravity = Gravity.BOTTOM;
        params.x = 0;
        params.y = 0;

        getWindow().setAttributes(params);
    }

    private void readData(){ // setelah di fix child untuk tim pake paramenter FirebaseCallback
        counterRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                counterPemain = Integer.parseInt(dataSnapshot.getValue().toString());
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

//    private interface FirebaseCallback{
//        void CounterPemain(int counterPemain);
//    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        String selectedJadwal = parent.getItemAtPosition(position).toString();
        jadwalBertanding = selectedJadwal;
        Toast.makeText(this, selectedJadwal, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {

    }
}
