package com.tim7.sparkuy;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import static com.tim7.sparkuy.RecyclerViewAdapter.NAMA_LAPANGAN;

public class detail_lapangan extends AppCompatActivity {
    Button bookingButotn;
    TextView namaLapangan, alamatLapangan;
    FirebaseDatabase firebaseDatabase;
    DatabaseReference ref;
    int counterPemain = 0;
    DatabaseReference pemainCounter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_lapangan);
        namaLapangan = findViewById(R.id.fieldTitle);
        alamatLapangan = findViewById(R.id.fieldAddress);
        Intent intent = getIntent();
        final String nama_lapangan = intent.getStringExtra(RecyclerViewAdapter.NAMA_LAPANGAN);
        String alamat_lapangan = intent.getStringExtra(RecyclerViewAdapter.ALAMAT_LAPANGAN);
        namaLapangan.setText(nama_lapangan);
        alamatLapangan.setText(alamat_lapangan);
        bookingButotn = findViewById(R.id.sparing_button);
        firebaseDatabase = FirebaseDatabase.getInstance();
        ref = firebaseDatabase.getReference("detail_pertandingan_lapangan").child(nama_lapangan);
        pemainCounter = FirebaseDatabase.getInstance().getReference("detail_pertandingan_lapangan").child(nama_lapangan).child("pemain_menunggu");
        pemainCounter.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                counterPemain = Integer.parseInt(dataSnapshot.getValue().toString());
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        bookingButotn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                counterPemain++;
                ref.child("pemain_menunggu").setValue(counterPemain);
                Intent intent = new Intent(detail_lapangan.this, JadwalPertandingan.class);
                intent.putExtra(NAMA_LAPANGAN, nama_lapangan);
                startActivity(intent);
            }
        });
    }
}
