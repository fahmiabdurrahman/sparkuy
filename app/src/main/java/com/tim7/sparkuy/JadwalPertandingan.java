package com.tim7.sparkuy;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import static com.tim7.sparkuy.RecyclerViewAdapter.NAMA_LAPANGAN;

public class JadwalPertandingan extends AppCompatActivity {
    FirebaseDatabase firebaseDatabase;
    DatabaseReference ref;
    Button tandingSolo, tandingTim;
    TextView onGoingTimA, onGoingScoreA, onGoingTimB, onGoingScoreB, waitingPlayer, namaTimBertanding, jadwalBertanding, jumlahBertanding;
    int counterPemain = 0;
    public static final String JADWAL_PERTANDINGAN = "com.tim7.sparkuy.jadwal_pertandingan";
    String jadwalPertanginganString;
    DatabaseReference pemainCounter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_jadwal_pertandingan);
        Intent intent = getIntent();
        final String nama_lapangan = intent.getStringExtra(RecyclerViewAdapter.NAMA_LAPANGAN);
        tandingSolo = findViewById(R.id.waitingMatchButton);
        tandingTim = findViewById(R.id.bookingLapanganTimButton);
        onGoingTimA = findViewById(R.id.timALabel);
        onGoingScoreA = findViewById(R.id.timAscore);
        onGoingTimB = findViewById(R.id.timBLabel);
        onGoingScoreB =findViewById(R.id.timBscore);
        waitingPlayer = findViewById(R.id.waitingPlayer);
        namaTimBertanding = findViewById(R.id.namatimbertanding);
        jadwalBertanding = findViewById(R.id.jadwalpertadinganwaiting);
        jumlahBertanding = findViewById(R.id.jumlahdibutuhkan);
        pemainCounter = FirebaseDatabase.getInstance().getReference("detail_pertandingan_lapangan").child(nama_lapangan).child("pemain_menunggu");
        ref = FirebaseDatabase.getInstance().getReference("detail_pertandingan_lapangan").child(nama_lapangan);
        readData(new jadwalCallback() {
            @Override
            public void jadwalPertandingan(String jadwal) {
                jadwalPertanginganString = jadwal;
            }
        });
        tandingSolo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(JadwalPertandingan.this, DetailPertandinganSolo.class);
                intent.putExtra(NAMA_LAPANGAN, nama_lapangan);
                intent.putExtra(JADWAL_PERTANDINGAN, jadwalPertanginganString);
                startActivity(intent);
            }
        });


        tandingTim.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(JadwalPertandingan.this, DetailPertandinganTim.class);
                intent.putExtra(NAMA_LAPANGAN, nama_lapangan);
                startActivity(intent);
            }
        });

        // membuat tampilan seperti pop up
        DisplayMetrics dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);

        int width = dm.widthPixels;
        int height = dm.heightPixels;

        getWindow().setLayout((int)(width), (int)(height*.8));

        WindowManager.LayoutParams params = getWindow().getAttributes();
        params.gravity = Gravity.BOTTOM;
        params.x = 0;
        params.y = 0;

        getWindow().setAttributes(params);
    }

    private void readData(final jadwalCallback jadwalCallback){
        DatabaseReference pemainCounter = ref.child("pemain_menunggu");
        pemainCounter.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                waitingPlayer.setText(dataSnapshot.getValue().toString());
                counterPemain = Integer.parseInt(dataSnapshot.getValue().toString());
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        DatabaseReference sedangBerlangsung = ref.child("sedang_berlangsung");
        DatabaseReference timA = sedangBerlangsung.child("tim_a");
        DatabaseReference namaTimA = timA.child("nama_tim");
        namaTimA.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                onGoingTimA.setText(dataSnapshot.getValue().toString());
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
        DatabaseReference skorTimA = timA.child("skor");
        skorTimA.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                onGoingScoreA.setText(dataSnapshot.getValue().toString());
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        DatabaseReference timB = sedangBerlangsung.child("tim_b");
        DatabaseReference namaTimB = timB.child("nama_tim");
        namaTimB.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                onGoingTimB.setText(dataSnapshot.getValue().toString());
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
        DatabaseReference skorTimB = timB.child("skor");
        skorTimB.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                onGoingScoreB.setText(dataSnapshot.getValue().toString());
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        DatabaseReference waitingMatchRef = ref.child("waiting_match");

        DatabaseReference jadwalRef = waitingMatchRef.child("jadwal_menunggu");
        jadwalRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                jadwalBertanding.setText(dataSnapshot.getValue().toString());
                jadwalCallback.jadwalPertandingan(jadwalBertanding.getText().toString());
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
        DatabaseReference butuhPemainRef = waitingMatchRef.child("membutuhkan_pemain");
        butuhPemainRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                jumlahBertanding.setText(dataSnapshot.getValue().toString());
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
        DatabaseReference timMenungguRef = waitingMatchRef.child("tim_menunggu");
        timMenungguRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                namaTimBertanding.setText(dataSnapshot.getValue().toString());
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }
    private interface jadwalCallback{
        void jadwalPertandingan(String jadwal);
    }
    @Override
    public void onBackPressed() {
        counterPemain--;
        if (counterPemain < 0){
            counterPemain = 0;
        }

        pemainCounter.setValue(counterPemain);
        super.onBackPressed();
        finish();
    }
}
