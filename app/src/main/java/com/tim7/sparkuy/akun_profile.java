package com.tim7.sparkuy;

import android.content.Intent;
import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.bumptech.glide.Glide;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;


public class akun_profile extends AppCompatActivity {

    private static final String TAG = "akun_profile";
    Button logoutButton, pengaturanButton;
    TextView namaPemain;
    ImageView fotoPemain;
    FirebaseAuth mAuth;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.tampilan_akun);
        Log.d(TAG, "onCreate: Starting.");
        mAuth = FirebaseAuth.getInstance();
        Button akun_pusat_bantuan = (Button) findViewById(R.id.akun_pusat_bantuan);
        Button akun_riwayat_pesanan = (Button) findViewById(R.id.akun_riwayat_pesanan);
        pengaturanButton = findViewById(R.id.pengaturanButton);

        akun_pusat_bantuan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(TAG, "onClick: akun_pusat_bantuan.");

                Intent intent = new Intent(akun_profile.this, pusat_bantuan.class);
                startActivity(intent);
            }
        });

        akun_riwayat_pesanan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(TAG, "onClick: akun_pusat_bantuan.");

                Intent intent = new Intent(akun_profile.this, riwayat_pemesanan.class);
                startActivity(intent);
            }
        });

        pengaturanButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(akun_profile.this, PengaturanActivity.class);
                startActivity(intent);
            }
        });

        BottomNavigationView bottomNavigationView = findViewById(R.id.bottom_navigation);

        bottomNavigationView.setSelectedItemId(R.id.nav_akun);

        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                switch (menuItem.getItemId()){
                    case R.id.nav_beranda:
                        startActivity(new Intent(getApplicationContext(), MainActivity.class));
                        return true;
                    case R.id.nav_sparing:
                        startActivity(new Intent(getApplicationContext(), pilih_lapangan_activity.class));
                        return true;
                    case R.id.nav_akun:
                        startActivity(new Intent(getApplicationContext(), akun_profile.class));
                        return true;
                }
                return false;
            }
        });
        ImageView imageview = (ImageView) findViewById(R.id.sparing_image);
        ColorMatrix matrix = new ColorMatrix();
        matrix.setSaturation(0);
        ColorMatrixColorFilter filter = new ColorMatrixColorFilter(matrix);
        imageview.setColorFilter(filter);
        namaPemain = findViewById(R.id.namaPemain);
        fotoPemain = findViewById(R.id.photoProfilePemain);
        logoutButton = findViewById(R.id.akun_logout);
        logoutButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FirebaseAuth.getInstance().signOut();
                Intent intent = new Intent(akun_profile.this, SplashScreenActivity.class);
                startActivity(intent);
                finish();
            }
        });
        loadUserInformation();
    }
    private void loadUserInformation() {
        FirebaseUser user = mAuth.getCurrentUser();
        DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference("User").child(user.getUid());
        DatabaseReference cekNama = databaseReference.child("nama");
        if (user != null) {
            if (user.getPhotoUrl() != null) {
                Glide.with(this)
                        .load(user.getPhotoUrl().toString())
                        .into(fotoPemain);
            }
            if (cekNama.getKey() != null) {
                cekNama.addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        namaPemain.setText(dataSnapshot.getValue().toString());
                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });
            }
        }
    }
    public void editProfileLayout(View view) {
        Intent intent = new Intent(this, edit_profile.class);
        startActivity(intent);
    }
}
